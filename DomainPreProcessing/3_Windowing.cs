﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DomainPreProcessing
{   
    class Windowing
    {
        public static void Run(string rootPath, bool copyAll = true, bool deleteWhenFinished = false)
        {
            string filePath = Path.Combine(rootPath, "Out2");
            string outPath = Path.Combine(rootPath, "Out3");
            Directory.CreateDirectory(outPath);
            DirectoryInfo DI = new DirectoryInfo(filePath);
            List<FileInfo> FAAfiles = DI.GetFiles().ToList();
            StringBuilder lines;
            string aaSeq = null;
            string contigHeader = null;
            int chunkLength = 200;
            int windowShift = 13;
            int seqCounter;
            string windowed;
            bool contigStart;
            string tempFileName = Path.Combine(outPath, "Temp.txt");
            string outfileName;

            if (File.Exists(tempFileName)) File.Delete(tempFileName);
            foreach (FileInfo file in FAAfiles)
            {
                seqCounter = 0;
                lines = new StringBuilder();
                StreamReader sR = new StreamReader(file.FullName);
                while (!sR.EndOfStream)
                {
                    string t = sR.ReadLine();

                    if (t.Contains(">"))
                    {
                        contigHeader = t;
                    }
                    else
                    {
                        t = t.Replace("*", "X");
                        aaSeq = t;
                        contigStart = false; // This is to ensure that there is something written for each contig
                        for (int i = 0; i < aaSeq.Length; i += windowShift == 0 ? chunkLength : windowShift)
                        {
                            windowed = aaSeq.Substring(i, Math.Min(chunkLength, aaSeq.Length - i));
                            if (contigStart && windowed.Length < chunkLength - 2 * windowShift) break;
                            lines.Append($"{contigHeader}[off{i}]" + "\r\n" + windowed + "\r\n");
                            seqCounter++;
                            contigStart = true;
                        }
                    }
                }
                sR.Close();
                WriteBuffer(tempFileName, lines);
                outfileName = Path.Combine(outPath, $"{Path.GetFileNameWithoutExtension(file.Name)}[w{chunkLength}][over{windowShift}][nseq{seqCounter}]" + ".FAA");
                File.Move(tempFileName, outfileName);
                Console.WriteLine(outfileName);
                if (deleteWhenFinished) file.Delete();
                string frame = file.Name.Split('[')[1][1].ToString();
                Tracking.Update(file.FullName, $"FAA Frame {frame} windowed : {DateTime.Now}");
            }

            if (copyAll) MoveAll(outPath, Path.Combine(rootPath, "3C_FAA"));
        }

        private static void WriteBuffer(string outFile, StringBuilder lines)
        {
            while (true)
            {
                try
                {
                    File.AppendAllText(outFile, lines.ToString());
                    break;
                }
                catch
                {
                    Console.WriteLine("Waiting For More Space");
                    for (int i = 0; i < 20 * 60; i++)
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                }
            }
        }

        public static void MoveAll (string fromFolder, string toFolder)
        {
            Directory.CreateDirectory(toFolder);
            string newName;
            foreach(FileInfo file in (new DirectoryInfo(fromFolder)).GetFiles())
            {
                newName = Path.Combine(toFolder, file.Name);
                file.MoveTo(newName);
            }
        }
    }
}
