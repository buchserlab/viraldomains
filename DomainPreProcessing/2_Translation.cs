﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainPreProcessing
{
    class Translation
    {
        public static void Run(string rootFolder, bool deleteAtEnd = false)
        {
            string inFNAFolder = Path.Combine(rootFolder, "Out1");
            string outFAAFolder = Path.Combine(rootFolder, "Out2");
            Directory.CreateDirectory(outFAAFolder);
            GenomesTranslate.SixFrameTranslate_Folder(inFNAFolder, outFAAFolder, false, deleteAtEnd);
        }
    }

    public static class GenomesTranslate
    {
        public static Dictionary<string, char> LookUp;
        public static Dictionary<char, char> Complement;
        public static HashSet<char> Standard;
       
        public static void Setup_Tables()
        {
            //https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi?chapter=cgencodes#SG11
            List<string> Bacterial_Code = new List<string>();
            Bacterial_Code.Add("FFLLSSSSYYAACCAWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG"); //result
            Bacterial_Code.Add("TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG"); //NT1
            Bacterial_Code.Add("TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG"); //NT2
            Bacterial_Code.Add("TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG"); //NT3

            LookUp = new Dictionary<string, char>(Bacterial_Code[0].Length);
            for (int i = 0; i < Bacterial_Code[0].Length; i++)
            {
                LookUp.Add(Bacterial_Code[1].Substring(i, 1) + Bacterial_Code[2].Substring(i, 1) + Bacterial_Code[3].Substring(i, 1),
                    Bacterial_Code[0][i]);
            }

            Complement = new Dictionary<char, char>(4) { { 'A', 'T' }, { 'T', 'A' }, { 'G', 'C' }, { 'C', 'G' } };
            Standard = new HashSet<char>() { 'A', 'G', 'C', 'T' };
        }

        public static void SixFrameTranslate_Folder(string FolderPath, string OutputPath, bool FastQ, bool deleteAtEnd)
        {
            if (LookUp == null) Setup_Tables();
            DirectoryInfo DI = new DirectoryInfo(FolderPath);
            List<FileInfo> FIs = DI.GetFiles().ToList();
            bool direct = true;
            int counter = 0;
            
            if (direct)
            {
                foreach (FileInfo FI in FIs)
                {
                    if (FastQ) SixFrameTranslate_FASTQ(FI.FullName);
                    else SixFrameTranslate_MultiPerFile(FI.FullName, OutputPath, FI.Name.ToString());
                    if (deleteAtEnd) FI.Delete();
                    Tracking.Update(FI.FullName, $"FNA Translated : {DateTime.Now}");
                }
            }
        }

        public static void SixFrameTranslate_MultiPerFile(string FilePath, string outPath, string FI)
        {
            Console.WriteLine((new FileInfo(FilePath)).Name);
            if (LookUp == null) Setup_Tables();
            string header;
            FastaSeq fastaSeq;
            string cleanedSequence;
            string SeqRes;
            int Frames = 6;
            int ProcessLength;
            string newPath;
            Debug.Print(FilePath);
            List<StringBuilder> FrameResults = new List<StringBuilder>(Frames);
            List<StringBuilder> OverallResults = new List<StringBuilder>(Frames);
            List<string> OutPaths = new List<string>(Frames);
            for (int i = 0; i < Frames; i++)
            {
                FrameResults.Add(null);
                OverallResults.Add(new StringBuilder());
                newPath = Path.Combine(outPath, $"{Path.GetFileNameWithoutExtension(FI)}[f{i}].faa");
                File.Delete(newPath);
                OutPaths.Add(newPath);
            }
            // Open and read through the source file one set of sequences at a time
            using (FastaReader fReader = new FastaReader(FilePath))
            {
                while ((fastaSeq = fReader.Next()) != null)
                {
                    for (int i = 0; i < Frames; i++)
                    {
                        FrameResults[i] = new StringBuilder();
                    }

                    header = fastaSeq.Name;
                    cleanedSequence = fastaSeq.Sequence.Replace("N", " ").Trim().Replace(" ", "N"); // This only works for one-sequence per line!
                    ProcessLength = 3 * (int)(Math.Round((double)(cleanedSequence.Length - 3) / 3) - 1);
                    Translate(cleanedSequence, ProcessLength, FrameResults, FilePath);
                    // Write out what we just made to each of the stream writers
                    for (int i = 0; i < Frames; i++)
                    {
                        SeqRes = FrameResults[i].ToString();
                        OverallResults[i].AppendLine($">{header}[f{i}][l{SeqRes.Length}]");
                        OverallResults[i].AppendLine(i < 3 ? SeqRes : Reverse(SeqRes));
                    }
                }
            }
            for (int i = 0; i < Frames; i++) File.AppendAllText(OutPaths[i], OverallResults[i].ToString());
        }

        public static void SixFrameTranslate_FASTQ(string FilePath)
        {
            if (LookUp == null) Setup_Tables();
            string Header;
            string tRead; string tSeq;
            int Frames = 6;
            int Total = 0; int ThrewAway = 0;
            Dictionary<string, int> SequencesWeHave = new Dictionary<string, int>();
            Dictionary<string, string> SeqToHeader = new Dictionary<string, string>();

            List<StreamWriter> SWs = new List<StreamWriter>(Frames);
            //Open and read through the source file one set of sequences at a time
            using (StreamReader SR = new StreamReader(FilePath))
            {
                while (!SR.EndOfStream)
                {
                    //Change Header Symbol from @ to >
                    Total++;
                    Header = SR.ReadLine().ToUpper();
                    tSeq = SR.ReadLine().ToUpper();

                    if (!SequencesWeHave.ContainsKey(tSeq))
                    {
                        SequencesWeHave.Add(tSeq, 1);

                        Header = Header.Replace("@", ">");
                        SeqToHeader.Add(tSeq, Header);
                    }
                    else
                    {
                        SequencesWeHave[tSeq]++;
                        ThrewAway++;
                    }
                    if (Total % 10000 == 0) Console.WriteLine(Total.ToString() + " Total, " + ThrewAway.ToString("0,000") + " Threw Out");


                    // Read next two lines - skip
                    tRead = SR.ReadLine();
                    tRead = SR.ReadLine();
                }
                SR.Close();
            }
            using (StreamWriter file = new StreamWriter(FilePath + ".TXT"))
                foreach (var entry in SequencesWeHave)
                    file.WriteLine("{0}\t{1}\t{2}", SeqToHeader[entry.Key], entry.Key, entry.Value);
        }

        public static void SixFrameTranslate(string FilePath)
        {
            if (LookUp == null) Setup_Tables();
            string Header;
            string Leftover = "";
            string tLine;
            int ProcessLength;
            List<StringBuilder> FrameResults = new List<StringBuilder>(6);
            for (int i = 0; i < FrameResults.Capacity; i++) FrameResults.Add(new StringBuilder());
            using (StreamReader SR = new StreamReader(FilePath))
            {
                Header = SR.ReadLine();
                while (!SR.EndOfStream)
                {
                    tLine = Leftover + SR.ReadLine().ToUpper();
                    if (tLine.Contains(">")) break; //Ignoring the last 4 bases
                    ProcessLength = 3 * (int)(Math.Round((double)(tLine.Length - 3) / 3) - 1);
                    Leftover = tLine.Substring(ProcessLength + 3);
                    Translate(tLine, ProcessLength, FrameResults, FilePath);
                }
                SR.Close();
            }
            string HeaderSimplified = Header;
            string FileHead = Path.GetFileNameWithoutExtension(FilePath);
            for (int i = 0; i < FrameResults.Count; i++)
            {
                File.WriteAllText(FilePath + "_" + i + ".faa",
                    HeaderSimplified + "_" + i + "\n" +
                    (i < 3 ? FrameResults[i].ToString() : Reverse(FrameResults[i].ToString())));
            }
        }

        public static string Reverse(string Forward)
        {
            char[] charArr = Forward.ToCharArray();
            Array.Reverse(charArr);
            return new string(charArr);
        }
        //Error Below
        public static void Translate(string Sequence, int ProcessLength, List<StringBuilder> SixFrames, string filePath)
        {
            try
            {
                string KeyF;
                string KeyR;
                for (int i = 0; i <= ProcessLength; i += 3)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        KeyF = Sequence.Substring(i + j, 3);
                        if (!LookUp.ContainsKey(KeyF))
                        {
                            Sequence = RemoveNonStandard(Sequence);
                            KeyF = Sequence.Substring(i + j, 3);
                        }
                        KeyR = "" + Complement[Sequence[i + j + 2]] + Complement[Sequence[i + j + 1]] + Complement[Sequence[i + j + 0]];
                        SixFrames[j + 0].Append(LookUp[KeyF]);
                        SixFrames[j + 3].Append(LookUp[KeyR]);
                    }
                }
            }
            catch (Exception)
            {

                Console.WriteLine(filePath);
            }
           
        }

        public static void Translate2(string Sequence, List<StreamWriter> SWs)
        {
            string KeyF;
            string KeyR;
            string SeqRes;
            Sequence = Sequence.Replace("N", "A");
            List<StringBuilder> SixFrames = new List<StringBuilder>(SWs.Count);
            for (int i = 0; i < SWs.Count; i++) SixFrames.Add(new StringBuilder());
            for (int i = 0; i <= Sequence.Length - 4; i += 3)
            {
                for (int j = 0; j < 3; j++)
                {
                    KeyF = Sequence.Substring(i + j, 3);
                    KeyR = "" + Complement[Sequence[i + j + 2]] + Complement[Sequence[i + j + 1]] + Complement[Sequence[i + j + 0]];
                    SixFrames[j + 0].Append(LookUp[KeyF]);
                    SixFrames[j + 3].Append(LookUp[KeyR]);
                }
            }

            for (int i = 0; i < SWs.Count; i++)
            {
                SeqRes = SixFrames[i].ToString();
                SWs[i].WriteLine(i < 3 ? SeqRes : Reverse(SeqRes)); //Reverse the squence if it is on the other strand
            }
        }

        public static string RemoveNonStandard(string OriginalSequence)
        {
            bool RemoveLeadingTrailingNs = true;
            string Trimmed = RemoveLeadingTrailingNs ? OriginalSequence.Replace("N", " ").Trim().Replace(" ", "N") : OriginalSequence; //Takes leading or trailing 'N's off first
            List<char> NonStandards = new List<char>();
            char[] charArr = Trimmed.ToCharArray();
            for (int i = 0; i < charArr.Length; i++)
                if (!Standard.Contains(charArr[i])) charArr[i] = 'A';
            return new string(charArr);
        }

        public static void CombineFASTAFiles(string FolderPath, string Search, int SetsOfFilesInNew)
        {
            //The 6 frames within each one will always be combined, this makes one set . . how many sets
            // - - First collect the sets
            DirectoryInfo DI = new DirectoryInfo(FolderPath);
            List<FileInfo> Files = DI.GetFiles(Search).ToList();
            Dictionary<string, List<FileInfo>> Sets = new Dictionary<string, List<FileInfo>>(Files.Count / 6);
            string Key;
            foreach (FileInfo FI in Files)
            {
                Key = FI.Name.Substring(0, FI.Name.Length - 5);
                if (!Sets.ContainsKey(Key)) Sets.Add(Key, new List<FileInfo>(6));
                Sets[Key].Add(FI);
            }
            // Now put the sets together into a new file
            int SetPointer = 0;
            int NewFileIndex = 0;
            StreamWriter SW = null; StreamReader SR;
            foreach (KeyValuePair<string, List<FileInfo>> KVP in Sets)
            {
                if (SetPointer % SetsOfFilesInNew == 0)
                {
                    if (NewFileIndex > 0) SW.Close();
                    SW = new StreamWriter(Path.Combine(DI.FullName, NewFileIndex++.ToString("00000") + ".fasta"));
                }
                SetPointer++;
                if (SetPointer >= Sets.Count) break;
                for (int i = 0; i < KVP.Value.Count; i++)
                {
                    SR = new StreamReader(KVP.Value[i].FullName);
                    SW.WriteLine(SR.ReadToEnd());
                    SR.Close();
                }
                if (SetPointer >= Sets.Count) break;
            }
            SW.Close();
        }

        public static void Split_PFAM_HMM(string CombinedHMM_FilePath, int HMMsPerFile)
        {
            StreamReader SR = new StreamReader(CombinedHMM_FilePath);
            int count = 0; int HMMsCount = 0;
            string t; StringBuilder section = new StringBuilder();
            while (!SR.EndOfStream)
            {
                t = SR.ReadLine();
                if (t.StartsWith("HMMER3/f "))
                {
                    if (HMMsCount % 2000 == 0) Debug.WriteLine(HMMsCount);
                    if (HMMsCount++ % HMMsPerFile == 0)
                    {
                        if (section.Length > 10)
                            Split_PFAM_HMM_Write(CombinedHMM_FilePath, section, count++);
                        section = new StringBuilder(t);
                    }
                    else
                    {
                        section.Append("\n" + t);
                    }
                }
                else
                {
                    section.Append("\n" + t);
                }
            }
            Split_PFAM_HMM_Write(CombinedHMM_FilePath, section, count++);
            SR.Close();
        }

        private static void Split_PFAM_HMM_Write(string CombinedHMM_FilePath, StringBuilder sB, int idx)
        {
            string newPath = Path.Combine(Path.GetDirectoryName(CombinedHMM_FilePath), idx.ToString()) + ".hmm";
            using (StreamWriter SW = new StreamWriter(newPath))
            {
                SW.Write(sB);
                SW.Close();
            }
        }

    }
}
