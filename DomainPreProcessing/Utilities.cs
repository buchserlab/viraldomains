﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DomainPreProcessing
{
    class GBKReader : IDisposable
    {
        public string Filename { get; }
        private StreamReader Reader;
        public bool EndofFile { get; private set; }
        private string _Line;
        private bool _ReadNext;
        public bool GetCDS;

        public GBKReader(string filename, bool getCDS = true)
        {
            Filename = filename;
            EndofFile = false;
            Reader = new StreamReader(Filename);
            _Line = Reader.ReadLine();
            _ReadNext = true;
            GetCDS = getCDS;
        }

        public GBKContig Next()
        {
            if (_Line == null)
            {
                EndofFile = true;
                return null;
            }
            if (!_Line.Contains("LOCUS")) throw new Exception("File Format is incorrect");
            var CDSList = new List<GBKCDS>();
            string contigSeq = "";
            string contigName = SpaceRemove(_Line);

            while ((_Line = _ReadNext ? Reader.ReadLine() : _Line) != null)
            {
                _ReadNext = true;
                if (_Line.Contains("LOCUS")) break;
                else if (_Line.Trim().StartsWith("ORIG")) contigSeq = GetContigSequence().ToUpper();
                else if (GetCDS && _Line.Trim().StartsWith("CDS  "))
                {
                    try
                    {
                        CDSList.Add(ReadCDS());
                    }
                    catch (FormatException e)
                    {
                        if (!(_Line.Contains("(total)") || _Line.Contains("coding")))
                        {
                            Console.WriteLine("Could not read CDS");
                        }
                    }
                }
            }
            return new GBKContig($"{Path.GetFileNameWithoutExtension(Filename).ToUpper()}[{contigName.ToUpper()}]", contigSeq, CDSList);
        }

        private string GetContigSequence()
        {
            var sb = new StringBuilder();
            while ((_Line = Reader.ReadLine()).Contains('1'))
            {
                if (_Line == "//") break; // This technically doesn't have to be here, but it's better safe than sorry
                sb.Append(Clean(_Line));
            }
            return sb.ToString();
        }

        public static Regex CDSNameMatcher = new Regex(@"(\d+)\.\.>?(\d+)");
        public static Regex CDSAttrMatcher = new Regex("\"([^\"]+)\""); // Weirdly, it's less confusing to escape quotations marks than use quotes in literal strings
        private GBKCDS ReadCDS()
        {
            string cdsSeq = "";
            int start = -1, stop = -1;
            bool strand = true;
            string id = "";

            string TrimL;

            if (_Line.Contains("complement")) strand = false;
            Match m = CDSNameMatcher.Match(_Line);
            start = int.Parse(m.Groups[1].Value);
            stop = int.Parse(m.Groups[2].Value);

            while ((_Line = Reader.ReadLine()) != null)
            {
                TrimL = _Line.Trim();
                if (TrimL.StartsWith("LOCUS  ")) throw new Exception("The CDS reader kept going to a new locus");
                if (TrimL.StartsWith("CDS  ") || TrimL.StartsWith("gene  ") || TrimL.StartsWith("tRNA  ") || TrimL.StartsWith("ORIG"))
                {
                    _ReadNext = false;
                    break; 
                }

                if (_Line.Contains("/locus_tag")) id = CDSAttrMatcher.Match(_Line).Groups[1].Value;
                else if (_Line.Contains("/translation"))
                {
                    cdsSeq = ReadCDSSeq();
                    break;
                }

            }
            return new GBKCDS(start, stop, strand, id, cdsSeq);
        }

        private string ReadCDSSeq()
        {
            string trimmedLine = SpaceRemove(_Line).Substring(14);
            if (trimmedLine.Contains('"'))
            {
                return trimmedLine.Substring(0, trimmedLine.Length - 1);
            }
            var sb = new StringBuilder(trimmedLine);
            while ((_Line = Reader.ReadLine()) != null)
            {
                if (trimmedLine.StartsWith("LOCUS")) throw new Exception("The CDSSeq reader kept going to a new locus");
                if (trimmedLine.StartsWith("CDS ") || trimmedLine.StartsWith("ORIG")) throw new Exception("The CDSSeq reader read past the end");
                trimmedLine = SpaceRemove(_Line);
                if (trimmedLine.Contains('"'))
                {
                    sb.Append(trimmedLine.Substring(0, trimmedLine.Length - 1));
                    break;
                }
                else
                {
                    sb.Append(trimmedLine);
                }
            }
            return sb.ToString();
        }

        public void Close()
        {
            Reader.Close();
        }
        public void Dispose()
        {
            Close();
            Reader.Dispose();
        }

        private static string SpaceRemove(string t)
        {
            t = t.Replace("     ", " ").Replace("  ", " ").Replace("  ", " ").Replace("  ", " ").Replace("  ", " ").Trim();
            t = t.Replace(" ", "_").Replace("____", "_").Replace("___", "_").Replace("__", "_");
            t = t.Replace(",", "");

            return t;
        }

        private static string Clean(string s)
        {
            s = Regex.Replace(s, @"[\d-]", string.Empty);
            s = s.Replace(" ", "");
            s = s.Replace("\t", "").Replace("\n", "").Replace("\r", "");
            return s;
        }
    }

    public struct GBKCDS
    {
        public int Start;
        public int Stop;
        public bool Strand;
        public string StrandStr { get => Strand ? "+" : "-"; }
        public string LocusTag;
        public string Sequence;
        public string FullName { get => $"[lid{LocusTag}][start{Start}][stop{Stop}][str{StrandStr}]"; }

        public GBKCDS(int start, int stop, bool strand, string locus_tag, string sequence)
        {
            Start = start;
            Stop = stop;
            Strand = strand;
            LocusTag = locus_tag;
            Sequence = sequence;
        }
    }

    class GBKContig
    {
        public string Name;
        public string ContigSeq;
        private List<GBKCDS> CodingSeqs;

        public GBKContig (string Name, string ContigSeq, List<GBKCDS> CodingSeqs)
        {
            this.Name = Name;
            this.ContigSeq = ContigSeq;
            this.CodingSeqs = CodingSeqs;
        }


        public List<FastaSeq> CDSToFasta()
        {
            return CodingSeqs.Select(s => new FastaSeq(Name + s.FullName, s.Sequence)).ToList();
        }
        public FastaSeq ContigToFasta()
        {
            return new FastaSeq(Name, ContigSeq);
        }
    }

    class FastaReader : IDisposable
    {
        public string Filename { get; }
        private StreamReader Reader;
        public bool EndofFile { get; private set; }
        private string _Line;
        private string _Name;
        private string _Seq;

        public FastaReader(string filename)
        {
            Filename = filename;
            Reader = new StreamReader(Filename);
            EndofFile = false;
            _Line = Reader.ReadLine();
        }

        public FastaSeq Next()
        {
            if (_Line == null)
            {
                EndofFile = true;
                return null;
            }
            _Name = _Line;
            StringBuilder sb = new StringBuilder();
            while ((_Line = Reader.ReadLine()) != null)
            {
                if (_Line.StartsWith('>')) break;
                sb.Append(_Line);
            }
            _Seq = sb.ToString();
            if (_Seq.Length == 0)
            {
                Console.WriteLine("The last sequence contained only a name");
            }
            if (!_Name.StartsWith('>')) throw new Exception("Something went wrong with reading the name");
            return new FastaSeq(_Name.Substring(1), _Seq);
        }

        public void Close()
        {
            Reader.Close();
        }
        public void Dispose()
        {
            Close();
            Reader.Dispose();
        }
    }

    class FastaWriter : IDisposable
    {
        private StreamWriter Writer;
        public string Filename;

        public FastaWriter(string filename)
        {
            Filename = filename;
            Writer = new StreamWriter(Filename);
        }

        public void WriteSeq(FastaSeq f)
        {
            Writer.WriteLine(f.ToString());
        }

        public void WriteSeqs(IEnumerable<FastaSeq> FastaSeqs)
        {
            foreach (var f in FastaSeqs)
            {
                WriteSeq(f);
            }
        }


        public void Close()
        {
            Writer.Close();
        }
        public void Dispose()
        {
            Close();
            Writer.Dispose();
        }
    }

    class FastaSeq
    {
        public string Name { get; set; }
        public string Sequence { get; set; }

        public FastaSeq(string Name, string Sequence)
        {
            this.Name = Name;
            this.Sequence = Sequence;
        }

        public override string ToString()
        {
            return $">{Name}" + Environment.NewLine + Sequence;
        }
    } 
    static class Tracking
    {
        internal static string TrackingLeaf = "tracking";
        public static void Update(string fullfilename, string message)
        {
            string trackfilename = Path.Combine(Directory.GetParent(fullfilename).Parent.FullName, TrackingLeaf, $"{GetGenome(fullfilename)}_track.txt");
            using (StreamWriter writer = File.AppendText(trackfilename))
            {
                writer.WriteLine(message);
            }
            return;
        }

        public static string GetGenome (string fullfilename)
        {
            return Path.GetFileNameWithoutExtension(fullfilename).Split('[')[0];
        }
    }
}

