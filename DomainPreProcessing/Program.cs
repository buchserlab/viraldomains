﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Xml.Serialization;

namespace DomainPreProcessing
{
    class Program
    {
        static void Main(string[] args)
        {
            // Start this code here. Running this will go through all steps of processing, aggregating, translating, and windowing GBK files. MainStep1 will porcess and aggregate
            // Step2 will 6 frame translate aggregated files
            // Step 3 will window translated files

            // All that you should need to do for setup is have the GBK files in a subfolder called InGBK, everything else will be set up 
            if (args.Length == 0)
            {
                Console.WriteLine("Please provide a root folder");
                return;
            }
            string rootFolder = args[0];
            Console.WriteLine(rootFolder);
            string trackingFolder = Path.Combine(rootFolder, "tracking");
            Directory.CreateDirectory(trackingFolder);
            ProcessAggregate.Run(rootFolder);
            Translation.Run(rootFolder);
            Windowing.Run(rootFolder);
        }
    }
}
