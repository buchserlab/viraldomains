﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DomainPreProcessing
{

    class ProcessAggregate
    {
        public static void Run(string rootFolder, bool deleteAtEnd = false)
        {
            bool getCDS = false;

            string GBKPath = Path.Combine(rootFolder, "InGBK");
            string contigOutFolder = Path.Combine(rootFolder, "Out1");
            string cdsOutFolder = Path.Combine(rootFolder, "Out1_CDS");
            
            Directory.CreateDirectory(contigOutFolder);
            if (getCDS) Directory.CreateDirectory(cdsOutFolder);

            DateTime date = DateTime.Now;
            string contigFilename;
            string cdsFilename;
            DirectoryInfo DI = new DirectoryInfo(GBKPath);
            List<FileInfo> GBKFiles = DI.GetFiles().Where(f => f.Name.ToUpper().EndsWith(".GBK")).ToList();
            

            StreamWriter CDSInfo = null;
            if (getCDS)
            {
                CDSInfo = new StreamWriter(Path.Combine(rootFolder, "CDSInfo.csv"));
                CDSInfo.WriteLine("Contig,CDS ID,Start,Stop,Strand");
            }

            StreamWriter GenomeLengthWriter = new StreamWriter(Path.Combine(rootFolder, "genomelengths.txt"));
            GenomeLengthWriter.WriteLine("Genome\tLength");
            int TotalGenomeLength;

            StreamWriter ContigLengthWriter = new StreamWriter(Path.Combine(rootFolder, "Contigs.csv"));
            ContigLengthWriter.WriteLine("Contig\tLength");
            
            GBKReader gbkReader;
            FastaWriter contigWriter;
            FastaWriter cdsWriter = null;


            foreach (FileInfo file in GBKFiles)
            {
                Console.WriteLine(file.Name);
                contigFilename = $"{Path.GetFileNameWithoutExtension(file.Name)}.FNA";
                cdsFilename = $"{Path.GetFileNameWithoutExtension(file.Name)}[CDS].FAA";
                
                gbkReader = new GBKReader(Path.Combine(GBKPath, file.Name), getCDS);
                contigWriter = new FastaWriter(Path.Combine(contigOutFolder, contigFilename));
                if (getCDS) cdsWriter = new FastaWriter(Path.Combine(cdsOutFolder, cdsFilename));

                TotalGenomeLength = 0;

                GBKContig contig;
                while((contig = gbkReader.Next()) != null)
                {
                    contigWriter.WriteSeq(contig.ContigToFasta());
                    TotalGenomeLength += contig.ContigSeq.Length;
                    ContigLengthWriter.WriteLine(ContigLine(contig));
                    if (getCDS)
                    {
                        List<FastaSeq> cdsSeqs = contig.CDSToFasta();
                        cdsWriter.WriteSeqs(cdsSeqs);
                        foreach (var cdsSeq in cdsSeqs) CDSInfo.WriteLine(ExtractCDSInfo(cdsSeq.Name.ToUpper()));
                    }
                }

                GenomeLengthWriter.WriteLine($"{Path.GetFileNameWithoutExtension(file.Name)}\t{TotalGenomeLength}");
                Tracking.Update(file.FullName, $"GBK Processed : {DateTime.Now}");
                gbkReader.Close();
                contigWriter.Close();
                if (getCDS) cdsWriter.Close();
                if (deleteAtEnd) file.Delete();
                if (getCDS) CDSInfo.Flush();
            }

            if (getCDS) CDSInfo.Close();
            ContigLengthWriter.Close();
            GenomeLengthWriter.Close();
        }

        private static string ContigLine(GBKContig contig)
        {
            string name = contig.Name.Replace('[', '|').Substring(0, contig.Name.Length - 1).ToUpper();
            int length = contig.ContigSeq.Length;
            return $"{name}\t{length}";
        }

        public static Regex rxGenome = new Regex(@"^([^[]+)");
        public static Regex rxInfo = new Regex(@"\[([^]]+)\]");


        private static string ExtractCDSInfo(string name)
        {
            name = name.ToUpper();
            string genome = rxGenome.Match(name).Groups[1].Value;
            string locus = "";
            string cdsID = "";
            string start = "";
            string stop = "";
            string strandString = "";

            string value;

            MatchCollection infoMatches = rxInfo.Matches(name);
            foreach (Match match in infoMatches)
            {
                value = match.Groups[1].Value;
                if (value.StartsWith("LOCUS")) locus = value;
                else if (value.StartsWith("LID")) cdsID = value.Substring(3);
                else if (value.StartsWith("START")) start = value.Substring(5);
                else if (value.StartsWith("STOP")) stop = value.Substring(4);
                else if (value.StartsWith("STR")) strandString = value.Substring(3);
            }

            if (locus.Length == 0) throw new Exception("Could not get locus information");
            if (cdsID.Length == 0) throw new Exception("Could not get id information");
            if (start.Length == 0) throw new Exception("Could not get start information");
            if (stop.Length == 0) throw new Exception("Could not get stop information");
            if (strandString.Length == 0) throw new Exception("Could not get strand information");

            return $"{genome}|{locus},{cdsID},{start},{stop},{strandString}";
        }
    }
}
