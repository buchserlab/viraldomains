﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace DomainAnalysis
{
    public class DomainClass
    {
        public short Query_ID;
        public int Start;
        public int Stop;
        public int QStart;
        /// <summary>
        /// true for the forward strand; false for the reverse strand
        /// </summary>
        public bool Strand;
        public byte Frame { get => (byte)((Start - 1) % 3 + 3 * (Strand ? 0 : 1)); }
        public string StrandStr { get => Strand ? "+" : "-"; }
        /// <summary>
        /// The true start of the domain, accounting for strand
        /// </summary>
        public int TrueStart { get => Strand ? Start : Stop; }
        /// <summary>
        /// The true stop of the domain, accounting for strand
        /// </summary>
        public int TrueStop { get => Strand ? Stop : Start; }
        public double iEValue;
        public string GeneID;
        private SortedList<double, DomainClass> InterestSet;
        public bool isInterest { get => InterestSet != null; }
        public static bool Use_isNeeded = true;
        private bool _isNeeded;
        public int Start_Presumed
        {
            get => Start - Math.Min(QStart, 20) + 1;
        }
        public int Length { get => Math.Abs(Stop - Start); }
        public bool isNeeded { get => Use_isNeeded ? _isNeeded : false; set => _isNeeded = value; }

        public DomainClass(string queryName, int start, int stop, int qstart, bool strand, double iEvalue, string GeneName, bool isNeeded, SortedList<double, DomainClass> InterestSet)
        {
            Query_ID = QueryName.GetIDFromName(queryName);
            Start = start; Stop = stop; Strand = strand; this.iEValue = iEvalue;
            GeneID = GeneName; this.isNeeded = isNeeded; QStart = qstart;
            this.InterestSet = InterestSet;
        }

        public override string ToString()
        {
            return Query_Name;
        }

        public string Query_Name { get { return QueryName.GetNameFromInt(Query_ID); } }

        private string _Clan = null;

        public string Clan
        {
            get
            {
                if (_Clan == null) loadClanRtype();
                return _Clan;
            }
        }

        public string rType
        {
            get
            {
                if (_rType == null)
                {
                    loadClanRtype();
                }
                return _rType;
            }
        }

        private void loadClanRtype()
        {
            string clan, rtype;
            Domain_Track_ForChromosome.DomainClanType(Query_Name, out clan, out rtype);
            _Clan = clan;
            _rType = rtype;
        }

        private string _rType = null;

        public bool isPfam
        {
            get => !(this.rType.ToUpper() == "ZZVFAM" || this.rType.ToUpper() == "ZZVOG");
        }

        internal void RemoveConnectedInterest()
        {
            if (!isInterest) return;
            if (InterestSet.Count > 0)
            {
            }
        }

        internal void AddToInterest(SortedList<double, DomainClass> sortedDomains)
        {
            if (!isInterest) return;
            int index = sortedDomains.IndexOfKey(this.Start);
            if (index + 1 < sortedDomains.Count)
            {
                DomainClass first = sortedDomains.Values[index + 1];
                if (first.Query_Name == this.Query_Name)
                    return;
            }
            InterestSet.Add(Start, this);
        }
        

        internal string Export(bool formatGFF = false)
        {
            if (formatGFF)
            {
                return "." + "\t" + "domain" + "\t" + Start + "\t" + Stop + "\t" + iEValue + "\t" + StrandStr + "\t" + "." + "\t" + $"Name={Query_Name};Qstart={QStart}";
            }
            return Query_Name + "\t" + Start + "\t" + Stop + "\t" + QStart + "\t" + StrandStr + "\t" + Frame + "\t" + iEValue;
        }

        internal static string ExportHeaders()
        {
            return "Query_Name" + "\t" + "Tfrom" + "\t" + "Tto" + "\t" + "Qfrom" + "\t" + "Strand" + "\t" + "Frame" + "\t" + "iEValue";
        }
    }

    public class QueryName
        {
            private static Dictionary<short, string> NameLookUp = new Dictionary<short, string>();
            private static Dictionary<string, short> IDLookUp = new Dictionary<string, short>();
            private static short Next_ID = 0;

            public static int Add(string queryName)
            {
                NameLookUp.Add(Next_ID, queryName);
                IDLookUp.Add(queryName, Next_ID);
                Next_ID++;
                return Next_ID - 1;
            }

            public static bool Contains(string queryName)
            {
                return IDLookUp.ContainsKey(queryName);
            }

            public static string GetNameFromInt(short Query_ID)
            {
                return NameLookUp[Query_ID];
            }

            public static short GetIDFromName(string queryName)
            {
                if (!QueryName.Contains(queryName)) QueryName.Add(queryName);
                return IDLookUp[queryName];
            }
        }

    public class Domain_Track_ForChromosome
    {
        public string Name;
        public SortedList<double, DomainClass> sortedDomains;
        
        public Dictionary<string, SortedList<double, DomainClass>> InterestEntries;
        public Dictionary<(string, int, string, int), byte> FrameTypeDict;

        public static int MaxDistBP_Save = 33000;
        public static int MaxDistBP_Save_NonNeed = MaxDistBP_Save / 10;
        public static int MinDistBP_InterestSave;
        public static bool KeepExactStartDomains = false;
        public static double FractionOverlapForRemoval_DifferentClan = 0.5; // Gets set below
        public static double FractionOverlapForRemoval_SameClan = 0.2; // Gets set below
        public static double LogDifferenceForOverlappingDomains_DifferentClan = 10; // Gets set below
        internal static bool GeneBased;

        public Domain_Track_ForChromosome(string Name)
        {
            this.Name = Name;
            sortedDomains = new SortedList<double, DomainClass>();
            InterestEntries = new Dictionary<string, SortedList<double, DomainClass>>();
            FrameTypeDict = new Dictionary<(string, int, string, int), byte>();
        }
        public void Add_Std(string Query, double iEValue, int Start, int Stop, int qStart, bool Strand, string GeneName_CanBeBlank, Query_Interest Interest)
        {
            double tStart = Start;
            if (sortedDomains.ContainsKey(tStart))
            {
                if (KeepExactStartDomains)
                {
                    Debugger.Break(); 
                    while (sortedDomains.ContainsKey(tStart)) { tStart += 0.01; }
                }
                else
                {
                    if (sortedDomains[tStart].iEValue < iEValue && sortedDomains[tStart].Query_Name == Query)
                    {
                        return; //Keep the existing one 
                    }
                    else
                    {
                        Delete_Domain(tStart);
                    }
                }
            }
            SortedList<double, DomainClass> InterestSet = null;
            if (Interest.Contains(Query))
            {
                string Type = Interest.GetInterestType(Query);
                if (!InterestEntries.ContainsKey(Type)) InterestEntries.Add(Type, new SortedList<double, DomainClass>());
                InterestSet = InterestEntries[Type];
            }
            sortedDomains.Add(tStart, new DomainClass(Query, Start, Stop, qStart, Strand, iEValue, GeneName_CanBeBlank, Interest.Need(Query), InterestSet));
        }

        public void Delete_Domain(double Position)
        {
            if (!sortedDomains.ContainsKey(Position)) return;
            DomainClass tDomain = sortedDomains[Position];
            tDomain.RemoveConnectedInterest();
            sortedDomains.Remove(Position);
        }

        public void Delete_Domains(ICollection<double> ToDeletePositions)
        {
            foreach (double Pos in ToDeletePositions)
            {
                Delete_Domain(Pos);
            }
        }

        private double SwapPoint(SortedList<double, DomainClass> sortedInterests, int Index)
        {
            if (Index + 1 < sortedInterests.Count)
            {
                return (sortedInterests.Keys[Index] + sortedInterests.Keys[Index + 1]) / 2;
            }
            else
            {
                return double.MaxValue;
            }
        }

        public bool SameDirection(byte Frame1, byte Frame2)
        {
            return ((Frame1 < 3 && Frame2 < 3) || (Frame1 >= 3 && Frame2 >= 3));
        }

        public void SetupFrameTypeDict()
        {
            (string, int, string, int) FrameKey; int CenterIdx; int tIdx; byte CenterFrame; byte tFrame; byte FrameType = 0;
            DomainClass NearDomain;
            FrameTypeDict = new Dictionary<(string, int, string, int), byte>();

            foreach (KeyValuePair<string, SortedList<double, DomainClass>> KVP in InterestEntries)
            {
                foreach (KeyValuePair<double, DomainClass> Interest in KVP.Value)
                {
                    // Go forward and backwards from this to find domains in the same frame and strand
                    CenterIdx = sortedDomains.IndexOfKey(Interest.Value.Start);
                    CenterFrame = Interest.Value.Frame;
                    if (Interest.Key != Interest.Value.Start)
                    {

                    }
                    FrameKey = GetFrameKey(Interest.Value, Interest.Value);
                    FrameType = 3;
                    if (!FrameTypeDict.ContainsKey(FrameKey))
                        FrameTypeDict.Add(FrameKey, FrameType);
                    for (int i = -1; i <= 1; i += 2)
                    {
                        FrameType = 3;
                        tIdx = CenterIdx; // Start with this frame, then step down and up to find the length of the same frame, same strand
                        do
                        {
                            tIdx = tIdx + i;
                            if (tIdx < 0 || tIdx >= sortedDomains.Count) break;
                            NearDomain = sortedDomains.Values[tIdx];
                            tFrame = NearDomain.Frame;
                            // 3 = Center Domain of Interest
                            // 2 = Same Frame as Center (just from center out, something in the same frame further away will not be counted
                            // 1 = Same Strand as Center
                            // 0 = Left the strand 
                            FrameType = Math.Min(FrameType, (byte)((CenterFrame == tFrame) ? 2 :
                                                                        SameDirection(CenterFrame, tFrame) ? 1 :
                                                                        0));
                            FrameKey = GetFrameKey(Interest.Value, NearDomain);
                            if (!FrameTypeDict.ContainsKey(FrameKey))
                            {
                                FrameTypeDict.Add(FrameKey, FrameType);
                            }
                            else
                            {

                            }
                        } while (FrameType > 0);
                    }
                }
            }
        }

        public static void ExportDistances_LineHeader(StreamWriter SW)
        {
            ExportDistances_Line(SW, "", null, "", null, 0, 0, 0, "");
        }

        public static void ExportDistances_Line(StreamWriter SW, string Target, DomainClass This, string InterestType, DomainClass Interest, double DistanceBetween, double DistBP, double DistDomains, string FrameType)
        {
            bool gb = Domain_Track_ForChromosome.GeneBased;
            if (This == null)
            {
                // Export the headers
                SW.WriteLine("Target" + "\t" + "int_Type" + "\t" + "int_QueryName" + "\t" + "int_tFrom" +
                    "\t" + "int_iEValue" + "\t" + "int_Frame" + (gb ? "\t" + "int_GeneID" : "") +
                    "\t" + "this_QueryName" + "\t" + "this_Clan" + "\t" + "this_RType" + "\t" + "this_iEValue" +
                    "\t" + "DistBP" + "\t" + "DistBPStarts" + "\t" + "DistDomains" + "\t" + "Frame" + "\t" + "FrameType" + (gb ? "\t" + "GeneID" : "") +
                    "\t" + "InvSqrBP" + "\t" + "InvSqrDomains" + "\t" + "hX" + "\t" + "hY");
            }
            else
            {
                string hx, hy; HilbertCoordinates(DistDomains, out hx, out hy);
                string clan, rType; DomainClanType(This.Query_Name, out clan, out rType);
                SW.WriteLine(Target + "\t" + InterestType + "\t" + Interest.Query_Name + "\t" + Interest.Start +
                    "\t" + Interest.iEValue + "\t" + Interest.Frame + (gb ? "\t" + Interest.GeneID : "") +
                    "\t" + This.Query_Name + "\t" + clan + "\t" + rType + "\t" + This.iEValue + "\t" + DistanceBetween + 
                    "\t" + DistBP + "\t" + DistDomains + "\t" + This.Frame + "\t" + FrameType + (gb ? "\t" + This.GeneID : "") +
                    "\t" + (1 / (1 + DistBP * DistBP)) + "\t" + (1 / (1 + DistDomains * DistDomains)) + "\t" + hx + "\t" + hy);
            }
        }

        public static string DomainClanDataPath;
        private static Dictionary<string, (string, string)> _DomainClanType = new Dictionary<string, (string, string)>();
        internal static void LoadInDomainClan(Dictionary<string, (string, string)> DomainClanTypeToLoad) { _DomainClanType = DomainClanTypeToLoad; }
        internal static void DomainClanType(string query_Name, out string clan, out string rType)
        {
            clan = ""; rType = "";
            string Key = query_Name;
            if (_DomainClanType.ContainsKey(Key))
            {
                (string, string) Res = _DomainClanType[Key];
                clan = Res.Item1; rType = Res.Item2;
            }
            else
            {

            }
        }

        private static Dictionary<int, (int x, int y)> _HilberCurve;

        private static void HilbertCoordinates(double distDomains, out string hx, out string hy)
        {
            if (_HilberCurve == null)
            {
                var h = new Dictionary<int, (int x, int y)>(1024);
                h.Add(-511, (16, -15)); h.Add(-510, (15, -15)); h.Add(-509, (15, -14)); h.Add(-508, (16, -14));
                h.Add(-507, (16, -13)); h.Add(-506, (16, -12)); h.Add(-505, (15, -12)); h.Add(-504, (15, -13));
                h.Add(-503, (14, -13)); h.Add(-502, (14, -12)); h.Add(-501, (13, -12)); h.Add(-500, (13, -13));
                h.Add(-499, (13, -14)); h.Add(-498, (14, -14)); h.Add(-497, (14, -15)); h.Add(-496, (13, -15));
                h.Add(-495, (12, -15)); h.Add(-494, (12, -14)); h.Add(-493, (11, -14)); h.Add(-492, (11, -15));
                h.Add(-491, (10, -15)); h.Add(-490, (9, -15)); h.Add(-489, (9, -14)); h.Add(-488, (10, -14));
                h.Add(-487, (10, -13)); h.Add(-486, (9, -13)); h.Add(-485, (9, -12)); h.Add(-484, (10, -12));
                h.Add(-483, (11, -12)); h.Add(-482, (11, -13)); h.Add(-481, (12, -13)); h.Add(-480, (12, -12));
                h.Add(-479, (12, -11)); h.Add(-478, (12, -10)); h.Add(-477, (11, -10)); h.Add(-476, (11, -11));
                h.Add(-475, (10, -11)); h.Add(-474, (9, -11)); h.Add(-473, (9, -10)); h.Add(-472, (10, -10));
                h.Add(-471, (10, -9)); h.Add(-470, (9, -9)); h.Add(-469, (9, -8)); h.Add(-468, (10, -8));
                h.Add(-467, (11, -8)); h.Add(-466, (11, -9)); h.Add(-465, (12, -9)); h.Add(-464, (12, -8));
                h.Add(-463, (13, -8)); h.Add(-462, (14, -8)); h.Add(-461, (14, -9)); h.Add(-460, (13, -9));
                h.Add(-459, (13, -10)); h.Add(-458, (13, -11)); h.Add(-457, (14, -11)); h.Add(-456, (14, -10));
                h.Add(-455, (15, -10)); h.Add(-454, (15, -11)); h.Add(-453, (16, -11)); h.Add(-452, (16, -10));
                h.Add(-451, (16, -9)); h.Add(-450, (15, -9)); h.Add(-449, (15, -8)); h.Add(-448, (16, -8));
                h.Add(-447, (16, -7)); h.Add(-446, (16, -6)); h.Add(-445, (15, -6)); h.Add(-444, (15, -7));
                h.Add(-443, (14, -7)); h.Add(-442, (13, -7)); h.Add(-441, (13, -6)); h.Add(-440, (14, -6));
                h.Add(-439, (14, -5)); h.Add(-438, (13, -5)); h.Add(-437, (13, -4)); h.Add(-436, (14, -4));
                h.Add(-435, (15, -4)); h.Add(-434, (15, -5)); h.Add(-433, (16, -5)); h.Add(-432, (16, -4));
                h.Add(-431, (16, -3)); h.Add(-430, (15, -3)); h.Add(-429, (15, -2)); h.Add(-428, (16, -2));
                h.Add(-427, (16, -1)); h.Add(-426, (16, 0)); h.Add(-425, (15, 0)); h.Add(-424, (15, -1));
                h.Add(-423, (14, -1)); h.Add(-422, (14, 0)); h.Add(-421, (13, 0)); h.Add(-420, (13, -1));
                h.Add(-419, (13, -2)); h.Add(-418, (14, -2)); h.Add(-417, (14, -3)); h.Add(-416, (13, -3));
                h.Add(-415, (12, -3)); h.Add(-414, (11, -3)); h.Add(-413, (11, -2)); h.Add(-412, (12, -2));
                h.Add(-411, (12, -1)); h.Add(-410, (12, 0)); h.Add(-409, (11, 0)); h.Add(-408, (11, -1));
                h.Add(-407, (10, -1)); h.Add(-406, (10, 0)); h.Add(-405, (9, 0)); h.Add(-404, (9, -1));
                h.Add(-403, (9, -2)); h.Add(-402, (10, -2)); h.Add(-401, (10, -3)); h.Add(-400, (9, -3));
                h.Add(-399, (9, -4)); h.Add(-398, (9, -5)); h.Add(-397, (10, -5)); h.Add(-396, (10, -4));
                h.Add(-395, (11, -4)); h.Add(-394, (12, -4)); h.Add(-393, (12, -5)); h.Add(-392, (11, -5));
                h.Add(-391, (11, -6)); h.Add(-390, (12, -6)); h.Add(-389, (12, -7)); h.Add(-388, (11, -7));
                h.Add(-387, (10, -7)); h.Add(-386, (10, -6)); h.Add(-385, (9, -6)); h.Add(-384, (9, -7));
                h.Add(-383, (8, -7)); h.Add(-382, (8, -6)); h.Add(-381, (7, -6)); h.Add(-380, (7, -7));
                h.Add(-379, (6, -7)); h.Add(-378, (5, -7)); h.Add(-377, (5, -6)); h.Add(-376, (6, -6));
                h.Add(-375, (6, -5)); h.Add(-374, (5, -5)); h.Add(-373, (5, -4)); h.Add(-372, (6, -4));
                h.Add(-371, (7, -4)); h.Add(-370, (7, -5)); h.Add(-369, (8, -5)); h.Add(-368, (8, -4));
                h.Add(-367, (8, -3)); h.Add(-366, (7, -3)); h.Add(-365, (7, -2)); h.Add(-364, (8, -2));
                h.Add(-363, (8, -1)); h.Add(-362, (8, 0)); h.Add(-361, (7, 0)); h.Add(-360, (7, -1));
                h.Add(-359, (6, -1)); h.Add(-358, (6, 0)); h.Add(-357, (5, 0)); h.Add(-356, (5, -1));
                h.Add(-355, (5, -2)); h.Add(-354, (6, -2)); h.Add(-353, (6, -3)); h.Add(-352, (5, -3));
                h.Add(-351, (4, -3)); h.Add(-350, (3, -3)); h.Add(-349, (3, -2)); h.Add(-348, (4, -2));
                h.Add(-347, (4, -1)); h.Add(-346, (4, 0)); h.Add(-345, (3, 0)); h.Add(-344, (3, -1));
                h.Add(-343, (2, -1)); h.Add(-342, (2, 0)); h.Add(-341, (1, 0)); h.Add(-340, (1, -1));
                h.Add(-339, (1, -2)); h.Add(-338, (2, -2)); h.Add(-337, (2, -3)); h.Add(-336, (1, -3));
                h.Add(-335, (1, -4)); h.Add(-334, (1, -5)); h.Add(-333, (2, -5)); h.Add(-332, (2, -4));
                h.Add(-331, (3, -4)); h.Add(-330, (4, -4)); h.Add(-329, (4, -5)); h.Add(-328, (3, -5));
                h.Add(-327, (3, -6)); h.Add(-326, (4, -6)); h.Add(-325, (4, -7)); h.Add(-324, (3, -7));
                h.Add(-323, (2, -7)); h.Add(-322, (2, -6)); h.Add(-321, (1, -6)); h.Add(-320, (1, -7));
                h.Add(-319, (1, -8)); h.Add(-318, (2, -8)); h.Add(-317, (2, -9)); h.Add(-316, (1, -9));
                h.Add(-315, (1, -10)); h.Add(-314, (1, -11)); h.Add(-313, (2, -11)); h.Add(-312, (2, -10));
                h.Add(-311, (3, -10)); h.Add(-310, (3, -11)); h.Add(-309, (4, -11)); h.Add(-308, (4, -10));
                h.Add(-307, (4, -9)); h.Add(-306, (3, -9)); h.Add(-305, (3, -8)); h.Add(-304, (4, -8));
                h.Add(-303, (5, -8)); h.Add(-302, (5, -9)); h.Add(-301, (6, -9)); h.Add(-300, (6, -8));
                h.Add(-299, (7, -8)); h.Add(-298, (8, -8)); h.Add(-297, (8, -9)); h.Add(-296, (7, -9));
                h.Add(-295, (7, -10)); h.Add(-294, (8, -10)); h.Add(-293, (8, -11)); h.Add(-292, (7, -11));
                h.Add(-291, (6, -11)); h.Add(-290, (6, -10)); h.Add(-289, (5, -10)); h.Add(-288, (5, -11));
                h.Add(-287, (5, -12)); h.Add(-286, (5, -13)); h.Add(-285, (6, -13)); h.Add(-284, (6, -12));
                h.Add(-283, (7, -12)); h.Add(-282, (8, -12)); h.Add(-281, (8, -13)); h.Add(-280, (7, -13));
                h.Add(-279, (7, -14)); h.Add(-278, (8, -14)); h.Add(-277, (8, -15)); h.Add(-276, (7, -15));
                h.Add(-275, (6, -15)); h.Add(-274, (6, -14)); h.Add(-273, (5, -14)); h.Add(-272, (5, -15));
                h.Add(-271, (4, -15)); h.Add(-270, (3, -15)); h.Add(-269, (3, -14)); h.Add(-268, (4, -14));
                h.Add(-267, (4, -13)); h.Add(-266, (4, -12)); h.Add(-265, (3, -12)); h.Add(-264, (3, -13));
                h.Add(-263, (2, -13)); h.Add(-262, (2, -12)); h.Add(-261, (1, -12)); h.Add(-260, (1, -13));
                h.Add(-259, (1, -14)); h.Add(-258, (2, -14)); h.Add(-257, (2, -15)); h.Add(-256, (1, -15));
                h.Add(-255, (0, -15)); h.Add(-254, (0, -14)); h.Add(-253, (-1, -14)); h.Add(-252, (-1, -15));
                h.Add(-251, (-2, -15)); h.Add(-250, (-3, -15)); h.Add(-249, (-3, -14)); h.Add(-248, (-2, -14));
                h.Add(-247, (-2, -13)); h.Add(-246, (-3, -13)); h.Add(-245, (-3, -12)); h.Add(-244, (-2, -12));
                h.Add(-243, (-1, -12)); h.Add(-242, (-1, -13)); h.Add(-241, (0, -13)); h.Add(-240, (0, -12));
                h.Add(-239, (0, -11)); h.Add(-238, (-1, -11)); h.Add(-237, (-1, -10)); h.Add(-236, (0, -10));
                h.Add(-235, (0, -9)); h.Add(-234, (0, -8)); h.Add(-233, (-1, -8)); h.Add(-232, (-1, -9));
                h.Add(-231, (-2, -9)); h.Add(-230, (-2, -8)); h.Add(-229, (-3, -8)); h.Add(-228, (-3, -9));
                h.Add(-227, (-3, -10)); h.Add(-226, (-2, -10)); h.Add(-225, (-2, -11)); h.Add(-224, (-3, -11));
                h.Add(-223, (-4, -11)); h.Add(-222, (-5, -11)); h.Add(-221, (-5, -10)); h.Add(-220, (-4, -10));
                h.Add(-219, (-4, -9)); h.Add(-218, (-4, -8)); h.Add(-217, (-5, -8)); h.Add(-216, (-5, -9));
                h.Add(-215, (-6, -9)); h.Add(-214, (-6, -8)); h.Add(-213, (-7, -8)); h.Add(-212, (-7, -9));
                h.Add(-211, (-7, -10)); h.Add(-210, (-6, -10)); h.Add(-209, (-6, -11)); h.Add(-208, (-7, -11));
                h.Add(-207, (-7, -12)); h.Add(-206, (-7, -13)); h.Add(-205, (-6, -13)); h.Add(-204, (-6, -12));
                h.Add(-203, (-5, -12)); h.Add(-202, (-4, -12)); h.Add(-201, (-4, -13)); h.Add(-200, (-5, -13));
                h.Add(-199, (-5, -14)); h.Add(-198, (-4, -14)); h.Add(-197, (-4, -15)); h.Add(-196, (-5, -15));
                h.Add(-195, (-6, -15)); h.Add(-194, (-6, -14)); h.Add(-193, (-7, -14)); h.Add(-192, (-7, -15));
                h.Add(-191, (-8, -15)); h.Add(-190, (-9, -15)); h.Add(-189, (-9, -14)); h.Add(-188, (-8, -14));
                h.Add(-187, (-8, -13)); h.Add(-186, (-8, -12)); h.Add(-185, (-9, -12)); h.Add(-184, (-9, -13));
                h.Add(-183, (-10, -13)); h.Add(-182, (-10, -12)); h.Add(-181, (-11, -12)); h.Add(-180, (-11, -13));
                h.Add(-179, (-11, -14)); h.Add(-178, (-10, -14)); h.Add(-177, (-10, -15)); h.Add(-176, (-11, -15));
                h.Add(-175, (-12, -15)); h.Add(-174, (-12, -14)); h.Add(-173, (-13, -14)); h.Add(-172, (-13, -15));
                h.Add(-171, (-14, -15)); h.Add(-170, (-15, -15)); h.Add(-169, (-15, -14)); h.Add(-168, (-14, -14));
                h.Add(-167, (-14, -13)); h.Add(-166, (-15, -13)); h.Add(-165, (-15, -12)); h.Add(-164, (-14, -12));
                h.Add(-163, (-13, -12)); h.Add(-162, (-13, -13)); h.Add(-161, (-12, -13)); h.Add(-160, (-12, -12));
                h.Add(-159, (-12, -11)); h.Add(-158, (-12, -10)); h.Add(-157, (-13, -10)); h.Add(-156, (-13, -11));
                h.Add(-155, (-14, -11)); h.Add(-154, (-15, -11)); h.Add(-153, (-15, -10)); h.Add(-152, (-14, -10));
                h.Add(-151, (-14, -9)); h.Add(-150, (-15, -9)); h.Add(-149, (-15, -8)); h.Add(-148, (-14, -8));
                h.Add(-147, (-13, -8)); h.Add(-146, (-13, -9)); h.Add(-145, (-12, -9)); h.Add(-144, (-12, -8));
                h.Add(-143, (-11, -8)); h.Add(-142, (-10, -8)); h.Add(-141, (-10, -9)); h.Add(-140, (-11, -9));
                h.Add(-139, (-11, -10)); h.Add(-138, (-11, -11)); h.Add(-137, (-10, -11)); h.Add(-136, (-10, -10));
                h.Add(-135, (-9, -10)); h.Add(-134, (-9, -11)); h.Add(-133, (-8, -11)); h.Add(-132, (-8, -10));
                h.Add(-131, (-8, -9)); h.Add(-130, (-9, -9)); h.Add(-129, (-9, -8)); h.Add(-128, (-8, -8));
                h.Add(-127, (-8, -7)); h.Add(-126, (-9, -7)); h.Add(-125, (-9, -6)); h.Add(-124, (-8, -6));
                h.Add(-123, (-8, -5)); h.Add(-122, (-8, -4)); h.Add(-121, (-9, -4)); h.Add(-120, (-9, -5));
                h.Add(-119, (-10, -5)); h.Add(-118, (-10, -4)); h.Add(-117, (-11, -4)); h.Add(-116, (-11, -5));
                h.Add(-115, (-11, -6)); h.Add(-114, (-10, -6)); h.Add(-113, (-10, -7)); h.Add(-112, (-11, -7));
                h.Add(-111, (-12, -7)); h.Add(-110, (-12, -6)); h.Add(-109, (-13, -6)); h.Add(-108, (-13, -7));
                h.Add(-107, (-14, -7)); h.Add(-106, (-15, -7)); h.Add(-105, (-15, -6)); h.Add(-104, (-14, -6));
                h.Add(-103, (-14, -5)); h.Add(-102, (-15, -5)); h.Add(-101, (-15, -4)); h.Add(-100, (-14, -4));
                h.Add(-99, (-13, -4)); h.Add(-98, (-13, -5)); h.Add(-97, (-12, -5)); h.Add(-96, (-12, -4));
                h.Add(-95, (-12, -3)); h.Add(-94, (-12, -2)); h.Add(-93, (-13, -2)); h.Add(-92, (-13, -3));
                h.Add(-91, (-14, -3)); h.Add(-90, (-15, -3)); h.Add(-89, (-15, -2)); h.Add(-88, (-14, -2));
                h.Add(-87, (-14, -1)); h.Add(-86, (-15, -1)); h.Add(-85, (-15, 0)); h.Add(-84, (-14, 0));
                h.Add(-83, (-13, 0)); h.Add(-82, (-13, -1)); h.Add(-81, (-12, -1)); h.Add(-80, (-12, 0));
                h.Add(-79, (-11, 0)); h.Add(-78, (-10, 0)); h.Add(-77, (-10, -1)); h.Add(-76, (-11, -1));
                h.Add(-75, (-11, -2)); h.Add(-74, (-11, -3)); h.Add(-73, (-10, -3)); h.Add(-72, (-10, -2));
                h.Add(-71, (-9, -2)); h.Add(-70, (-9, -3)); h.Add(-69, (-8, -3)); h.Add(-68, (-8, -2));
                h.Add(-67, (-8, -1)); h.Add(-66, (-9, -1)); h.Add(-65, (-9, 0)); h.Add(-64, (-8, 0));
                h.Add(-63, (-7, 0)); h.Add(-62, (-7, -1)); h.Add(-61, (-6, -1)); h.Add(-60, (-6, 0));
                h.Add(-59, (-5, 0)); h.Add(-58, (-4, 0)); h.Add(-57, (-4, -1)); h.Add(-56, (-5, -1));
                h.Add(-55, (-5, -2)); h.Add(-54, (-4, -2)); h.Add(-53, (-4, -3)); h.Add(-52, (-5, -3));
                h.Add(-51, (-6, -3)); h.Add(-50, (-6, -2)); h.Add(-49, (-7, -2)); h.Add(-48, (-7, -3));
                h.Add(-47, (-7, -4)); h.Add(-46, (-6, -4)); h.Add(-45, (-6, -5)); h.Add(-44, (-7, -5));
                h.Add(-43, (-7, -6)); h.Add(-42, (-7, -7)); h.Add(-41, (-6, -7)); h.Add(-40, (-6, -6));
                h.Add(-39, (-5, -6)); h.Add(-38, (-5, -7)); h.Add(-37, (-4, -7)); h.Add(-36, (-4, -6));
                h.Add(-35, (-4, -5)); h.Add(-34, (-5, -5)); h.Add(-33, (-5, -4)); h.Add(-32, (-4, -4));
                h.Add(-31, (-3, -4)); h.Add(-30, (-2, -4)); h.Add(-29, (-2, -5)); h.Add(-28, (-3, -5));
                h.Add(-27, (-3, -6)); h.Add(-26, (-3, -7)); h.Add(-25, (-2, -7)); h.Add(-24, (-2, -6));
                h.Add(-23, (-1, -6)); h.Add(-22, (-1, -7)); h.Add(-21, (0, -7)); h.Add(-20, (0, -6));
                h.Add(-19, (0, -5)); h.Add(-18, (-1, -5)); h.Add(-17, (-1, -4)); h.Add(-16, (0, -4));
                h.Add(-15, (0, -3)); h.Add(-14, (0, -2)); h.Add(-13, (-1, -2)); h.Add(-12, (-1, -3));
                h.Add(-11, (-2, -3)); h.Add(-10, (-3, -3)); h.Add(-9, (-3, -2)); h.Add(-8, (-2, -2));
                h.Add(-7, (-2, -1)); h.Add(-6, (-3, -1)); h.Add(-5, (-3, 0)); h.Add(-4, (-2, 0));
                h.Add(-3, (-1, 0)); h.Add(-2, (-1, -1)); h.Add(-1, (0, -1)); h.Add(0, (0, 0));
                h.Add(1, (0, 1)); h.Add(2, (0, 2)); h.Add(3, (-1, 2)); h.Add(4, (-1, 1));
                h.Add(5, (-2, 1)); h.Add(6, (-3, 1)); h.Add(7, (-3, 2)); h.Add(8, (-2, 2));
                h.Add(9, (-2, 3)); h.Add(10, (-3, 3)); h.Add(11, (-3, 4)); h.Add(12, (-2, 4));
                h.Add(13, (-1, 4)); h.Add(14, (-1, 3)); h.Add(15, (0, 3)); h.Add(16, (0, 4));
                h.Add(17, (0, 5)); h.Add(18, (-1, 5)); h.Add(19, (-1, 6)); h.Add(20, (0, 6));
                h.Add(21, (0, 7)); h.Add(22, (0, 8)); h.Add(23, (-1, 8)); h.Add(24, (-1, 7));
                h.Add(25, (-2, 7)); h.Add(26, (-2, 8)); h.Add(27, (-3, 8)); h.Add(28, (-3, 7));
                h.Add(29, (-3, 6)); h.Add(30, (-2, 6)); h.Add(31, (-2, 5)); h.Add(32, (-3, 5));
                h.Add(33, (-4, 5)); h.Add(34, (-5, 5)); h.Add(35, (-5, 6)); h.Add(36, (-4, 6));
                h.Add(37, (-4, 7)); h.Add(38, (-4, 8)); h.Add(39, (-5, 8)); h.Add(40, (-5, 7));
                h.Add(41, (-6, 7)); h.Add(42, (-6, 8)); h.Add(43, (-7, 8)); h.Add(44, (-7, 7));
                h.Add(45, (-7, 6)); h.Add(46, (-6, 6)); h.Add(47, (-6, 5)); h.Add(48, (-7, 5));
                h.Add(49, (-7, 4)); h.Add(50, (-7, 3)); h.Add(51, (-6, 3)); h.Add(52, (-6, 4));
                h.Add(53, (-5, 4)); h.Add(54, (-4, 4)); h.Add(55, (-4, 3)); h.Add(56, (-5, 3));
                h.Add(57, (-5, 2)); h.Add(58, (-4, 2)); h.Add(59, (-4, 1)); h.Add(60, (-5, 1));
                h.Add(61, (-6, 1)); h.Add(62, (-6, 2)); h.Add(63, (-7, 2)); h.Add(64, (-7, 1));
                h.Add(65, (-8, 1)); h.Add(66, (-9, 1)); h.Add(67, (-9, 2)); h.Add(68, (-8, 2));
                h.Add(69, (-8, 3)); h.Add(70, (-8, 4)); h.Add(71, (-9, 4)); h.Add(72, (-9, 3));
                h.Add(73, (-10, 3)); h.Add(74, (-10, 4)); h.Add(75, (-11, 4)); h.Add(76, (-11, 3));
                h.Add(77, (-11, 2)); h.Add(78, (-10, 2)); h.Add(79, (-10, 1)); h.Add(80, (-11, 1));
                h.Add(81, (-12, 1)); h.Add(82, (-12, 2)); h.Add(83, (-13, 2)); h.Add(84, (-13, 1));
                h.Add(85, (-14, 1)); h.Add(86, (-15, 1)); h.Add(87, (-15, 2)); h.Add(88, (-14, 2));
                h.Add(89, (-14, 3)); h.Add(90, (-15, 3)); h.Add(91, (-15, 4)); h.Add(92, (-14, 4));
                h.Add(93, (-13, 4)); h.Add(94, (-13, 3)); h.Add(95, (-12, 3)); h.Add(96, (-12, 4));
                h.Add(97, (-12, 5)); h.Add(98, (-12, 6)); h.Add(99, (-13, 6)); h.Add(100, (-13, 5));
                h.Add(101, (-14, 5)); h.Add(102, (-15, 5)); h.Add(103, (-15, 6)); h.Add(104, (-14, 6));
                h.Add(105, (-14, 7)); h.Add(106, (-15, 7)); h.Add(107, (-15, 8)); h.Add(108, (-14, 8));
                h.Add(109, (-13, 8)); h.Add(110, (-13, 7)); h.Add(111, (-12, 7)); h.Add(112, (-12, 8));
                h.Add(113, (-11, 8)); h.Add(114, (-10, 8)); h.Add(115, (-10, 7)); h.Add(116, (-11, 7));
                h.Add(117, (-11, 6)); h.Add(118, (-11, 5)); h.Add(119, (-10, 5)); h.Add(120, (-10, 6));
                h.Add(121, (-9, 6)); h.Add(122, (-9, 5)); h.Add(123, (-8, 5)); h.Add(124, (-8, 6));
                h.Add(125, (-8, 7)); h.Add(126, (-9, 7)); h.Add(127, (-9, 8)); h.Add(128, (-8, 8));
                h.Add(129, (-8, 9)); h.Add(130, (-9, 9)); h.Add(131, (-9, 10)); h.Add(132, (-8, 10));
                h.Add(133, (-8, 11)); h.Add(134, (-8, 12)); h.Add(135, (-9, 12)); h.Add(136, (-9, 11));
                h.Add(137, (-10, 11)); h.Add(138, (-10, 12)); h.Add(139, (-11, 12)); h.Add(140, (-11, 11));
                h.Add(141, (-11, 10)); h.Add(142, (-10, 10)); h.Add(143, (-10, 9)); h.Add(144, (-11, 9));
                h.Add(145, (-12, 9)); h.Add(146, (-12, 10)); h.Add(147, (-13, 10)); h.Add(148, (-13, 9));
                h.Add(149, (-14, 9)); h.Add(150, (-15, 9)); h.Add(151, (-15, 10)); h.Add(152, (-14, 10));
                h.Add(153, (-14, 11)); h.Add(154, (-15, 11)); h.Add(155, (-15, 12)); h.Add(156, (-14, 12));
                h.Add(157, (-13, 12)); h.Add(158, (-13, 11)); h.Add(159, (-12, 11)); h.Add(160, (-12, 12));
                h.Add(161, (-12, 13)); h.Add(162, (-12, 14)); h.Add(163, (-13, 14)); h.Add(164, (-13, 13));
                h.Add(165, (-14, 13)); h.Add(166, (-15, 13)); h.Add(167, (-15, 14)); h.Add(168, (-14, 14));
                h.Add(169, (-14, 15)); h.Add(170, (-15, 15)); h.Add(171, (-15, 16)); h.Add(172, (-14, 16));
                h.Add(173, (-13, 16)); h.Add(174, (-13, 15)); h.Add(175, (-12, 15)); h.Add(176, (-12, 16));
                h.Add(177, (-11, 16)); h.Add(178, (-10, 16)); h.Add(179, (-10, 15)); h.Add(180, (-11, 15));
                h.Add(181, (-11, 14)); h.Add(182, (-11, 13)); h.Add(183, (-10, 13)); h.Add(184, (-10, 14));
                h.Add(185, (-9, 14)); h.Add(186, (-9, 13)); h.Add(187, (-8, 13)); h.Add(188, (-8, 14));
                h.Add(189, (-8, 15)); h.Add(190, (-9, 15)); h.Add(191, (-9, 16)); h.Add(192, (-8, 16));
                h.Add(193, (-7, 16)); h.Add(194, (-7, 15)); h.Add(195, (-6, 15)); h.Add(196, (-6, 16));
                h.Add(197, (-5, 16)); h.Add(198, (-4, 16)); h.Add(199, (-4, 15)); h.Add(200, (-5, 15));
                h.Add(201, (-5, 14)); h.Add(202, (-4, 14)); h.Add(203, (-4, 13)); h.Add(204, (-5, 13));
                h.Add(205, (-6, 13)); h.Add(206, (-6, 14)); h.Add(207, (-7, 14)); h.Add(208, (-7, 13));
                h.Add(209, (-7, 12)); h.Add(210, (-6, 12)); h.Add(211, (-6, 11)); h.Add(212, (-7, 11));
                h.Add(213, (-7, 10)); h.Add(214, (-7, 9)); h.Add(215, (-6, 9)); h.Add(216, (-6, 10));
                h.Add(217, (-5, 10)); h.Add(218, (-5, 9)); h.Add(219, (-4, 9)); h.Add(220, (-4, 10));
                h.Add(221, (-4, 11)); h.Add(222, (-5, 11)); h.Add(223, (-5, 12)); h.Add(224, (-4, 12));
                h.Add(225, (-3, 12)); h.Add(226, (-2, 12)); h.Add(227, (-2, 11)); h.Add(228, (-3, 11));
                h.Add(229, (-3, 10)); h.Add(230, (-3, 9)); h.Add(231, (-2, 9)); h.Add(232, (-2, 10));
                h.Add(233, (-1, 10)); h.Add(234, (-1, 9)); h.Add(235, (0, 9)); h.Add(236, (0, 10));
                h.Add(237, (0, 11)); h.Add(238, (-1, 11)); h.Add(239, (-1, 12)); h.Add(240, (0, 12));
                h.Add(241, (0, 13)); h.Add(242, (0, 14)); h.Add(243, (-1, 14)); h.Add(244, (-1, 13));
                h.Add(245, (-2, 13)); h.Add(246, (-3, 13)); h.Add(247, (-3, 14)); h.Add(248, (-2, 14));
                h.Add(249, (-2, 15)); h.Add(250, (-3, 15)); h.Add(251, (-3, 16)); h.Add(252, (-2, 16));
                h.Add(253, (-1, 16)); h.Add(254, (-1, 15)); h.Add(255, (0, 15)); h.Add(256, (0, 16));
                h.Add(257, (1, 16)); h.Add(258, (2, 16)); h.Add(259, (2, 15)); h.Add(260, (1, 15));
                h.Add(261, (1, 14)); h.Add(262, (1, 13)); h.Add(263, (2, 13)); h.Add(264, (2, 14));
                h.Add(265, (3, 14)); h.Add(266, (3, 13)); h.Add(267, (4, 13)); h.Add(268, (4, 14));
                h.Add(269, (4, 15)); h.Add(270, (3, 15)); h.Add(271, (3, 16)); h.Add(272, (4, 16));
                h.Add(273, (5, 16)); h.Add(274, (5, 15)); h.Add(275, (6, 15)); h.Add(276, (6, 16));
                h.Add(277, (7, 16)); h.Add(278, (8, 16)); h.Add(279, (8, 15)); h.Add(280, (7, 15));
                h.Add(281, (7, 14)); h.Add(282, (8, 14)); h.Add(283, (8, 13)); h.Add(284, (7, 13));
                h.Add(285, (6, 13)); h.Add(286, (6, 14)); h.Add(287, (5, 14)); h.Add(288, (5, 13));
                h.Add(289, (5, 12)); h.Add(290, (5, 11)); h.Add(291, (6, 11)); h.Add(292, (6, 12));
                h.Add(293, (7, 12)); h.Add(294, (8, 12)); h.Add(295, (8, 11)); h.Add(296, (7, 11));
                h.Add(297, (7, 10)); h.Add(298, (8, 10)); h.Add(299, (8, 9)); h.Add(300, (7, 9));
                h.Add(301, (6, 9)); h.Add(302, (6, 10)); h.Add(303, (5, 10)); h.Add(304, (5, 9));
                h.Add(305, (4, 9)); h.Add(306, (3, 9)); h.Add(307, (3, 10)); h.Add(308, (4, 10));
                h.Add(309, (4, 11)); h.Add(310, (4, 12)); h.Add(311, (3, 12)); h.Add(312, (3, 11));
                h.Add(313, (2, 11)); h.Add(314, (2, 12)); h.Add(315, (1, 12)); h.Add(316, (1, 11));
                h.Add(317, (1, 10)); h.Add(318, (2, 10)); h.Add(319, (2, 9)); h.Add(320, (1, 9));
                h.Add(321, (1, 8)); h.Add(322, (1, 7)); h.Add(323, (2, 7)); h.Add(324, (2, 8));
                h.Add(325, (3, 8)); h.Add(326, (4, 8)); h.Add(327, (4, 7)); h.Add(328, (3, 7));
                h.Add(329, (3, 6)); h.Add(330, (4, 6)); h.Add(331, (4, 5)); h.Add(332, (3, 5));
                h.Add(333, (2, 5)); h.Add(334, (2, 6)); h.Add(335, (1, 6)); h.Add(336, (1, 5));
                h.Add(337, (1, 4)); h.Add(338, (2, 4)); h.Add(339, (2, 3)); h.Add(340, (1, 3));
                h.Add(341, (1, 2)); h.Add(342, (1, 1)); h.Add(343, (2, 1)); h.Add(344, (2, 2));
                h.Add(345, (3, 2)); h.Add(346, (3, 1)); h.Add(347, (4, 1)); h.Add(348, (4, 2));
                h.Add(349, (4, 3)); h.Add(350, (3, 3)); h.Add(351, (3, 4)); h.Add(352, (4, 4));
                h.Add(353, (5, 4)); h.Add(354, (6, 4)); h.Add(355, (6, 3)); h.Add(356, (5, 3));
                h.Add(357, (5, 2)); h.Add(358, (5, 1)); h.Add(359, (6, 1)); h.Add(360, (6, 2));
                h.Add(361, (7, 2)); h.Add(362, (7, 1)); h.Add(363, (8, 1)); h.Add(364, (8, 2));
                h.Add(365, (8, 3)); h.Add(366, (7, 3)); h.Add(367, (7, 4)); h.Add(368, (8, 4));
                h.Add(369, (8, 5)); h.Add(370, (8, 6)); h.Add(371, (7, 6)); h.Add(372, (7, 5));
                h.Add(373, (6, 5)); h.Add(374, (5, 5)); h.Add(375, (5, 6)); h.Add(376, (6, 6));
                h.Add(377, (6, 7)); h.Add(378, (5, 7)); h.Add(379, (5, 8)); h.Add(380, (6, 8));
                h.Add(381, (7, 8)); h.Add(382, (7, 7)); h.Add(383, (8, 7)); h.Add(384, (8, 8));
                h.Add(385, (9, 8)); h.Add(386, (9, 7)); h.Add(387, (10, 7)); h.Add(388, (10, 8));
                h.Add(389, (11, 8)); h.Add(390, (12, 8)); h.Add(391, (12, 7)); h.Add(392, (11, 7));
                h.Add(393, (11, 6)); h.Add(394, (12, 6)); h.Add(395, (12, 5)); h.Add(396, (11, 5));
                h.Add(397, (10, 5)); h.Add(398, (10, 6)); h.Add(399, (9, 6)); h.Add(400, (9, 5));
                h.Add(401, (9, 4)); h.Add(402, (10, 4)); h.Add(403, (10, 3)); h.Add(404, (9, 3));
                h.Add(405, (9, 2)); h.Add(406, (9, 1)); h.Add(407, (10, 1)); h.Add(408, (10, 2));
                h.Add(409, (11, 2)); h.Add(410, (11, 1)); h.Add(411, (12, 1)); h.Add(412, (12, 2));
                h.Add(413, (12, 3)); h.Add(414, (11, 3)); h.Add(415, (11, 4)); h.Add(416, (12, 4));
                h.Add(417, (13, 4)); h.Add(418, (14, 4)); h.Add(419, (14, 3)); h.Add(420, (13, 3));
                h.Add(421, (13, 2)); h.Add(422, (13, 1)); h.Add(423, (14, 1)); h.Add(424, (14, 2));
                h.Add(425, (15, 2)); h.Add(426, (15, 1)); h.Add(427, (16, 1)); h.Add(428, (16, 2));
                h.Add(429, (16, 3)); h.Add(430, (15, 3)); h.Add(431, (15, 4)); h.Add(432, (16, 4));
                h.Add(433, (16, 5)); h.Add(434, (16, 6)); h.Add(435, (15, 6)); h.Add(436, (15, 5));
                h.Add(437, (14, 5)); h.Add(438, (13, 5)); h.Add(439, (13, 6)); h.Add(440, (14, 6));
                h.Add(441, (14, 7)); h.Add(442, (13, 7)); h.Add(443, (13, 8)); h.Add(444, (14, 8));
                h.Add(445, (15, 8)); h.Add(446, (15, 7)); h.Add(447, (16, 7)); h.Add(448, (16, 8));
                h.Add(449, (16, 9)); h.Add(450, (15, 9)); h.Add(451, (15, 10)); h.Add(452, (16, 10));
                h.Add(453, (16, 11)); h.Add(454, (16, 12)); h.Add(455, (15, 12)); h.Add(456, (15, 11));
                h.Add(457, (14, 11)); h.Add(458, (14, 12)); h.Add(459, (13, 12)); h.Add(460, (13, 11));
                h.Add(461, (13, 10)); h.Add(462, (14, 10)); h.Add(463, (14, 9)); h.Add(464, (13, 9));
                h.Add(465, (12, 9)); h.Add(466, (12, 10)); h.Add(467, (11, 10)); h.Add(468, (11, 9));
                h.Add(469, (10, 9)); h.Add(470, (9, 9)); h.Add(471, (9, 10)); h.Add(472, (10, 10));
                h.Add(473, (10, 11)); h.Add(474, (9, 11)); h.Add(475, (9, 12)); h.Add(476, (10, 12));
                h.Add(477, (11, 12)); h.Add(478, (11, 11)); h.Add(479, (12, 11)); h.Add(480, (12, 12));
                h.Add(481, (12, 13)); h.Add(482, (12, 14)); h.Add(483, (11, 14)); h.Add(484, (11, 13));
                h.Add(485, (10, 13)); h.Add(486, (9, 13)); h.Add(487, (9, 14)); h.Add(488, (10, 14));
                h.Add(489, (10, 15)); h.Add(490, (9, 15)); h.Add(491, (9, 16)); h.Add(492, (10, 16));
                h.Add(493, (11, 16)); h.Add(494, (11, 15)); h.Add(495, (12, 15)); h.Add(496, (12, 16));
                h.Add(497, (13, 16)); h.Add(498, (14, 16)); h.Add(499, (14, 15)); h.Add(500, (13, 15));
                h.Add(501, (13, 14)); h.Add(502, (13, 13)); h.Add(503, (14, 13)); h.Add(504, (14, 14));
                h.Add(505, (15, 14)); h.Add(506, (15, 13)); h.Add(507, (16, 13)); h.Add(508, (16, 14));
                h.Add(509, (16, 15)); h.Add(510, (15, 15)); h.Add(511, (15, 16)); h.Add(512, (16, 16));
                _HilberCurve = h;
            }
            int Key = (int)distDomains;
            if (_HilberCurve.ContainsKey(Key))
            {
                (int Resx, int Resy) = _HilberCurve[Key];
                hx = Resx.ToString(); hy = Resy.ToString();
            }
            else
            {
                hx = ""; hy = "";
            }
        }

        public void ExportOrig(StreamWriter SW, bool formatGFF = false)
        {
            foreach (KeyValuePair<double, DomainClass> Domain in sortedDomains)
            {
                SW.WriteLine(this.Name + "\t" + Domain.Value.Export(formatGFF));
            }
        }

        public void ExportDistances(StreamWriter SW, bool UseSwapPoint)
        {
            double swapPoint;
            double Dist_BP; double Dist_Domains;
            int Interest_Idx_Pointer; double Interest_Pointer = 0;
            int DomainIndex_Interest = 0; int DomainIndex_This;
            double DistanceBetween;
            DomainClass CurrentInterest = null;
            (string, int, string, int) FrameKey; byte FrameType;

            SetupFrameTypeDict();

            foreach (KeyValuePair<string, SortedList<double, DomainClass>> KVP in InterestEntries)
            {
                Interest_Idx_Pointer = -1;
                if (UseSwapPoint)
                {
                    DomainIndex_This = -1; swapPoint = -1;
                    foreach (KeyValuePair<double, DomainClass> Domain in sortedDomains)
                    {
                        DomainIndex_This++;
                        if (Domain.Key > swapPoint) // Check if the pointer is correct
                        {
                            Interest_Idx_Pointer++;
                            Interest_Pointer = KVP.Value.Keys[Interest_Idx_Pointer];
                            CurrentInterest = KVP.Value.Values[Interest_Idx_Pointer];
                            DomainIndex_Interest = sortedDomains.IndexOfKey(Interest_Pointer);
                            swapPoint = SwapPoint(KVP.Value, Interest_Idx_Pointer);
                        }
                        Dist_BP = Domain.Key - Interest_Pointer;
                        if (Math.Abs(Dist_BP) < MaxDistBP_Save)
                        {
                            Dist_Domains = DomainIndex_This - DomainIndex_Interest;
                            FrameKey = GetFrameKey(CurrentInterest, Domain.Value);
                            if (FrameTypeDict.ContainsKey(FrameKey)) { FrameType = FrameTypeDict[FrameKey]; }
                            else { FrameType = 0; }
                            ExportDistances_Line(SW, Name, Domain.Value, KVP.Key, CurrentInterest, 0, Dist_BP, Dist_Domains, FrameType.ToString());
                        }
                    }
                }
                else
                {
                    foreach (KeyValuePair<double, DomainClass> InterestDomain in KVP.Value)
                    {
                        DomainIndex_This = -1; Interest_Idx_Pointer++;
                        Interest_Pointer = InterestDomain.Key;
                        CurrentInterest = InterestDomain.Value;
                        DomainIndex_Interest = sortedDomains.IndexOfKey(Interest_Pointer);
                        if (DomainIndex_Interest < 0)
                        {

                        }
                        foreach (KeyValuePair<double, DomainClass> Domain in sortedDomains)
                        {
                            DomainIndex_This++;
                            DistanceBetween = Domain.Key - InterestDomain.Key;
                            Dist_BP = Domain.Value.TrueStart - InterestDomain.Value.TrueStart; // True Start is contig referenced (not 5'-3' reference and accounts for domains on the negative strand being the other direction)
                            if (Math.Abs(Dist_BP) < (InterestDomain.Value.isNeeded ? MaxDistBP_Save : MaxDistBP_Save_NonNeed))
                            {
                                Dist_Domains = DomainIndex_This - DomainIndex_Interest;
                                if (!InterestDomain.Value.Strand) // If interested domain is on the negative strand, then flip the distances so that everything is from the interested domains point of view
                                {
                                    DistanceBetween = -1 * DistanceBetween;
                                    Dist_BP = -1 * Dist_BP;
                                    Dist_Domains = -1 * Dist_Domains;
                                }
                                    FrameKey = GetFrameKey(CurrentInterest, Domain.Value);
                                if (FrameTypeDict.ContainsKey(FrameKey)) { FrameType = FrameTypeDict[FrameKey]; }
                                else { FrameType = 0; }
                                ExportDistances_Line(SW, Name, Domain.Value, KVP.Key, CurrentInterest, DistanceBetween, Dist_BP, Dist_Domains, FrameType.ToString());
                            }
                        }
                    }
                }
            }

            FrameTypeDict = null;
        }

        public static (string, int, string, int) GetFrameKey(DomainClass Interest, DomainClass Nearby)
        {
            return (Interest.Query_Name, Interest.Start, Nearby.Query_Name, Nearby.Start);
        }

        public void RemoveOverlapping()
        {
            //Important notes:
            // 1. Look at overlap on a per-frame basis (each frame separately)
            // 2. Look at overlap with real start and end and also the calculated start (where the domain would normally start)
            // 3a. Same clan - Only allow overlap of < 33%
            // 3b. Different clans - Only allow overlap of < 45% if one domain is 10 logs better than the other in iEValue
            // 4. Consider nearest neighbor domains AND 'skip-1' neighbors  (ABC, consider A-B, B-C, AND A-C)
            int StartingCount = sortedDomains.Count; int ThisFrameCount;
            KeyValuePair<double, DomainClass> Previous1, Previous2;
            if (sortedDomains.Count <= 1)
                return;
            HashSet<double> ToDelete;
            for (int tFrame = 0; tFrame < 6; tFrame++)
            {
                do
                {
                    ThisFrameCount = 0;
                    ToDelete = new HashSet<double>();
                    Previous1 = new KeyValuePair<double, DomainClass>(double.NaN, null);
                    Previous2 = new KeyValuePair<double, DomainClass>(double.NaN, null);
                    foreach (KeyValuePair<double, DomainClass> KVP in sortedDomains)
                    {
                        if (KVP.Value.Frame == tFrame)
                        {
                            ThisFrameCount++;
                            if (!double.IsNaN(Previous1.Key)) RemoveOverlap_Internal(KVP, Previous1, ToDelete);
                            if (!double.IsNaN(Previous2.Key)) RemoveOverlap_Internal(KVP, Previous2, ToDelete);

                            Previous2 = Previous1; Previous1 = KVP;
                        }
                    }
                    Delete_Domains(ToDelete);
                } while (ToDelete.Count > 0);
            }
        }

        private void RemoveOverlap_Internal(KeyValuePair<double, DomainClass> KVP, KeyValuePair<double, DomainClass> Previous, HashSet<double> ToDelete)
        {
            double Overlap; double LogDiff; double FractionOverlapUse;
            if ((KVP.Value.Start < Previous.Value.Stop) || (KVP.Value.Start_Presumed < Previous.Value.Stop))
            {
                Overlap = OverlapCalculate(KVP, Previous);
                if (ClansLikelySame(KVP.Value, Previous.Value))
                {
                    // If they are in the same clan, remove one of the two, whichever has the best e-value, even if there is only a small overlap
                    FractionOverlapUse = FractionOverlapForRemoval_SameClan;
                }
                else
                {
                    LogDiff = Math.Log10(Math.Max(1E-100, KVP.Value.iEValue)) - Math.Log10(Math.Max(1E-100, Previous.Value.iEValue));
                    if (KVP.Value.isPfam ^ Previous.Value.isPfam) // If one domain is from PFAM and the other is not
                    {
                        if (LogDiff * (KVP.Value.isPfam ? -1 : 1) <= -5)
                        {
                            FractionOverlapUse = FractionOverlapForRemoval_DifferentClan;
                        }
                        else
                        {
                            if (Overlap >= FractionOverlapForRemoval_DifferentClan)
                            {
                                ToDelete.Add((KVP.Value.isPfam ? Previous.Key : KVP.Key));
                            }
                            return;
                        }
                    }
                    // If both domains are fairly significant, then keep them both
                    else if (Math.Max(KVP.Value.iEValue, Previous.Value.iEValue) < 1E-10)
                    {
                        return;
                    }
                    // If they are in different clans, remove one if the e-value is really bad or they e-values are REALLY different
                    else if (Math.Abs(LogDiff) > LogDifferenceForOverlappingDomains_DifferentClan || Math.Max(KVP.Value.iEValue, Previous.Value.iEValue) > 0.001)
                    {
                        FractionOverlapUse = FractionOverlapForRemoval_DifferentClan;
                    }
                    else
                    {
                        FractionOverlapUse = 2000;
                    }
                }

                if (Overlap >= FractionOverlapUse)
                {
                    KeyValuePair<double, DomainClass> Better, Worse;
                    BetterWorse(Previous, KVP, out Better, out Worse);
                    if (Worse.Value.isNeeded && !Better.Value.isNeeded)
                    {
                    }
                    else
                    {
                        ToDelete.Add(Worse.Key);
                    }
                }
            }
        }

        private bool ClansLikelySame(DomainClass v1, DomainClass v2)
        {
            if (v1.Clan == v2.Clan) return true;
            if (v1.Query_Name.Substring(0, Math.Min(v1.Query_Name.Length, 3)) == v2.Query_Name.Substring(0, Math.Min(v2.Query_Name.Length, 3)))
            {
                if (v1.Query_Name.Substring(0, 3) == "DUF") return false; else return true;
            }
            return false;
        }

        private static double OverlapCalculate(KeyValuePair<double, DomainClass> KVP, KeyValuePair<double, DomainClass> Previous)
        {
            double Over = Math.Min(Previous.Value.Stop, KVP.Value.Stop) - Math.Min(KVP.Value.Start, KVP.Value.Start_Presumed);
            double Overlap = Over / Math.Min(Previous.Value.Stop - Previous.Value.Start, KVP.Value.Stop - KVP.Value.Start);
            return Math.Min(1, Overlap);
        }

        private static void BetterWorse(KeyValuePair<double, DomainClass> Previous, KeyValuePair<double, DomainClass> KVP, out KeyValuePair<double, DomainClass> Better, out KeyValuePair<double, DomainClass> Worse)
        {
            if (KVP.Value.iEValue == Previous.Value.iEValue)
            {
                Better = KVP.Value.Length > Previous.Value.Length ? KVP : Previous;
                Worse = KVP.Value.Length > Previous.Value.Length ? Previous : KVP;
            }
            else
            {
                Better = KVP.Value.iEValue < Previous.Value.iEValue ? KVP : Previous;
                Worse = KVP.Value.iEValue < Previous.Value.iEValue ? Previous : KVP;
            }
        }
    }

    public class Query_Interest
        {
            private HashSet<string> HS_Interest;
            private Dictionary<string, string> DomainToType;
            private Dictionary<string, List<string>> TypeToDomains;
            private HashSet<string> HS_Need;

            public Query_Interest()
            {
                HS_Interest = new HashSet<string>();
                HS_Need = new HashSet<string>();
                DomainToType = new Dictionary<string, string>();
                TypeToDomains = new Dictionary<string, List<string>>();
            }

            internal static Query_Interest LoadDomains()
            {
                string path = Domain_Track_ForChromosome.DomainClanDataPath;
                Query_Interest T = new Query_Interest();
                var DomainClanType = new Dictionary<string, (string, string)>();
                FileInfo FI = new FileInfo(path);
                if (!FI.Exists) return T;
                string[] Lines = File.ReadAllLines(path);
                string[] cols;
                for (int i = 1; i < Lines.Length; i++)
                {
                    cols = Lines[i].Split('\t');
                    DomainClanType.Add(cols[0], (cols[1], cols[2]));
                    if (cols[6] != "")
                    {
                        T.Add(cols[0], cols[6], cols[7] == "1");
                    }
                }
                Domain_Track_ForChromosome.LoadInDomainClan(DomainClanType);
                return T;
            }

            
            public void Add(string Key, string Type)
            {
                Add(Key, Type, false);
            }

            public void Add(string Key, string Type, bool Need)
            {
                Key = Key.Trim().ToUpper();
                HS_Interest.Add(Key);
                if (Need) HS_Need.Add(Key);
                DomainToType.Add(Key, Type);
                if (!TypeToDomains.ContainsKey(Type))
                {
                    TypeToDomains.Add(Type, new List<string>());
                }
                TypeToDomains[Type].Add(Key);
            }

            public bool Contains(string Key)
            {
                return HS_Interest.Contains(Key.ToUpper().Trim());
            }

            public string GetInterestType(string Key)
            {
                return DomainToType[Key.ToUpper().Trim()];
            }

            public bool Need(string Key)
            {
                return HS_Need.Contains(Key.ToUpper().Trim());
            }
        }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    public static class HMM_Helper
    {
        public static string[] SplitTab = new string[1] { "\t" };
        public static string[] SplitPipe = new string[1] { "|" };

        public static void Distances(string rootFolder, Dictionary<string, int> DictLength, string compiledFile = null)
        {
            bool UseSwapPoint = false; // true means that one interest will cut off the next, false gives the full neighborhood around the interest (none-unique)
            double Max_iEValue = 1; // Used to be 10
            Domain_Track_ForChromosome.GeneBased = false;
            Domain_Track_ForChromosome.MaxDistBP_Save = 32000;
            Domain_Track_ForChromosome.MaxDistBP_Save_NonNeed = Domain_Track_ForChromosome.MaxDistBP_Save / 1; // Usually 5
            Domain_Track_ForChromosome.FractionOverlapForRemoval_DifferentClan = 0.45;
            Domain_Track_ForChromosome.FractionOverlapForRemoval_SameClan = 0.33;
            Domain_Track_ForChromosome.LogDifferenceForOverlappingDomains_DifferentClan = 10;
            Domain_Track_ForChromosome.MinDistBP_InterestSave = 50;  // Was 60
            DomainClass.Use_isNeeded = false; // Usually true
            Domain_Track_ForChromosome.DomainClanDataPath = Path.Combine(rootFolder, "Pfamdata.txt"); 
            string Type = "RefGen01"; 

            // Uses the most recent text file that doesn't contain "Trimmed" as the compiled
            string PathCompiled = compiledFile ?? (new DirectoryInfo(rootFolder)).GetFiles()
                                                                                 .Where(f => f.Name.EndsWith("txt"))
                                                                                 .Where(f => !Path.GetFileNameWithoutExtension(f.Name).EndsWith("Trimmed"))
                                                                                 .OrderByDescending(f => f.LastWriteTime)
                                                                                 .ToList()[0]
                                                                                 .FullName;

            Query_Interest Interest = Query_Interest.LoadDomains(); 
            if (!File.Exists(PathCompiled)) Debugger.Break();
            
            Debug.Print("Setup Length Dict. Anything in the HMM files that AREN'T found in the length file WONT be exported.  They will go into the missing file in temp.");

            Dictionary<string, Domain_Track_ForChromosome> Large;
            HashSet<string> TargetMissingTrack;
            ExtractDomainsFromCompiled(Max_iEValue, ref PathCompiled, Interest, DictLength, out TargetMissingTrack, out Large);
            Debug.Print("Done Reading..");
            if (TargetMissingTrack.Count > 0) File.WriteAllText($@"c:\temp\missing_{Type}_New.txt", string.Join("\r\n", TargetMissingTrack));

            Debug.Print("About to Delete Extras .."); //Then remove any that don't have Interest
            List<string> ToDelete = new List<string>();
            foreach (KeyValuePair<string, Domain_Track_ForChromosome> KVP in Large)
            {
                if (KVP.Value.InterestEntries.Count < 1)
                    ToDelete.Add(KVP.Key);
            }
            Debug.Print($"Deleting {ToDelete.Count} Genomes.");
            foreach (string Chrom in ToDelete) Large.Remove(Chrom);
            Debug.Print("Removing Overlapping ..");

             
            RemoveOverlapping(Large.Values.ToList());
            ExportTrimmed(Large, PathCompiled, formatGFF: false); // <--------------------------------
            BuildInterests(Large);

            Debug.Print("Exporting .."); //Now go through and get the data
            string NewPath = PathCompiled.Replace("_Compiled", "_DomainSpacingA"); if (NewPath == PathCompiled) NewPath = Path.Combine(rootFolder, $"{Path.GetFileNameWithoutExtension(PathCompiled)}_DomainSpacing.txt");
            StreamWriter SW = new StreamWriter(NewPath);
            Domain_Track_ForChromosome.ExportDistances_LineHeader(SW);
            int Counter = 0;
            foreach (KeyValuePair<string, Domain_Track_ForChromosome> KVP in Large)
            {
                if ((Counter++) % 50 == 0) Debug.Print(KVP.Key + " " + (100 * Counter / Large.Count) + "%");
                KVP.Value.ExportDistances(SW, UseSwapPoint);
            }
            SW.Close();
            Debugger.Break();
        }

        private static void ExportTrimmed(Dictionary<string, Domain_Track_ForChromosome> large, string PathCompiled, bool formatGFF = false)
        {
            string outFolder = Path.GetDirectoryName(PathCompiled);
            string NewPath = PathCompiled.Replace("_Compiled", "_Trimmed");
            if (NewPath == PathCompiled) NewPath = Path.Combine(outFolder, Path.GetFileNameWithoutExtension(PathCompiled) + "_Trimmed.txt");
            if (formatGFF) NewPath = Path.Combine(outFolder, $"{Path.GetFileNameWithoutExtension(NewPath)}.gff");
            StreamWriter SW = new StreamWriter(NewPath);
            if (formatGFF)
            {
                SW.WriteLine("##gff-version 3");
            }
            else
            {
                SW.WriteLine("Contig" + "\t" + DomainClass.ExportHeaders());
            }
            foreach (KeyValuePair<string, Domain_Track_ForChromosome> KVP in large)
            {
                KVP.Value.ExportOrig(SW, formatGFF);
            }
            SW.Close();
        }

        private static void BuildInterests(Dictionary<string, Domain_Track_ForChromosome> large)
        {
            foreach (KeyValuePair<string, Domain_Track_ForChromosome> KVP in large)
            {
                foreach (KeyValuePair<double, DomainClass> Set in KVP.Value.sortedDomains)
                {
                    Set.Value.AddToInterest(KVP.Value.sortedDomains);
                }
                List<string> ToDelete = new List<string>();
                foreach (KeyValuePair<string, SortedList<double, DomainClass>> Types in KVP.Value.InterestEntries)
                {
                    if (Types.Value.Count == 0) ToDelete.Add(Types.Key);
                }
                foreach (string item in ToDelete) KVP.Value.InterestEntries.Remove(item);
            }
        }

        private static void ExtractDomainsFromCompiled(double Max_iEValue, ref string PathCompiled, Query_Interest Interest, Dictionary<string, int> DictLength, out HashSet<string> TargetMissingTrack, out Dictionary<string, Domain_Track_ForChromosome> Large, int NStandardSeq = 10000)
        {
            bool GeneBased = Domain_Track_ForChromosome.GeneBased;
            TargetMissingTrack = new HashSet<string>();
            Large = new Dictionary<string, Domain_Track_ForChromosome>();
            Domain_Track_ForChromosome tChrom = null;
            int cIdx_File = 0;
            int cIdx_Target = 1, cIdx_Query = 2;
            int cIdx_EValue = 5;
            int cIdx_QStart = 7;
            int cIdx_Start = 9, cIdx_Stop = cIdx_Start + 1;
            string HMMOutFile;
            string Query, Target;
            int genomeLength;
            int contigLength = -1;
            int Frame = -1;
            int offset = -1;
            bool Strand = true;
            double iEvalue; string ContigUnique; string GeneName = "";
            string genome, contigName;
            int Start, Stop;
            int tempstart;
            int qStart;
            string[] arr; string[] headers; string[] parr = null;

            Regex rxContig = new Regex(@"^([^[]+)\[([^]]+)\]");
            Match contigMatch;
            string targetName;
            Regex rxInfo = new Regex(@"\[([A-Za-z]+)(\d+)\]");
            MatchCollection targetMatches;
            MatchCollection fileMatches;

            Debug.Print("Copying Compiled.."); //This is the start of the Reading-in Process of the Huge Compiled file
            PathCompiled = CopyToLocal(PathCompiled);
            Debug.Print("Reading Compiled..");
            using (StreamReader SR = new StreamReader(PathCompiled))
            {
                headers = SR.ReadLine().Split(SplitTab, StringSplitOptions.None);
                while (!SR.EndOfStream)
                {
                    Frame = -1;
                    contigLength = -1;
                    offset = -1;
                    Strand = true;

                    arr = SR.ReadLine().Split(SplitTab, StringSplitOptions.None);
                    iEvalue = double.Parse(arr[cIdx_EValue]);

                    HMMOutFile = arr[cIdx_File].ToUpper();
                    fileMatches = rxInfo.Matches(HMMOutFile);

                    Target = arr[cIdx_Target].ToUpper();
                    Query = arr[cIdx_Query].ToUpper();
                    Start = int.Parse(arr[cIdx_Start]);
                    Stop = int.Parse(arr[cIdx_Stop]);
                    if (!int.TryParse(arr[cIdx_QStart], out qStart)) qStart = 0;

                    targetMatches = rxInfo.Matches(Target);

                    foreach (Match m in targetMatches)
                    {
                        switch (m.Groups[1].Value)
                        {

                            case "F":
                                Frame = int.Parse(m.Groups[2].Value);
                                break;
                            case "L":
                                contigLength = int.Parse(m.Groups[2].Value);
                                break;
                            case "OFF":
                                offset = int.Parse(m.Groups[2].Value);
                                break;
                            default:
                                Console.WriteLine($"Unused information {m.Groups[2].Value}");
                                Debugger.Break();
                                break;
                        }
                    }

                    if (Frame == -1) throw new Exception("Target doesn't have frame information");
                    if (contigLength == -1) throw new Exception("Target doesn't have contig length information");
                    if (offset == -1) throw new Exception("Target doesn't have offset information");

                    Start += offset;
                    Stop += offset;
                    if (Frame >= 3) // Adjusts the start and stop position if it is on the reverse strand and swaps the start and stop values
                    {
                        Strand = false;
                        tempstart = Start;
                        Start = contigLength - Stop + 1;
                        Stop = contigLength - tempstart + 1;
                    }

                    // Converts amino acid position to nucleotide position
                    Start = (3 * (Start - 1)) + (Frame % 3) + 1;
                    Stop = (3 * (Stop - 1)) + (Frame % 3) + 1;

                    contigMatch = rxContig.Match(Target);
                    genome = contigMatch.Groups[1].Value;
                    contigName = contigMatch.Groups[2].Value;
                    genomeLength = DictLength[genome];

                    if (!contigName.Contains("LOCUS")) throw new Exception("Target doesn't have contig information");

                    foreach (Match m in fileMatches)
                    {
                        if (m.Groups[1].Value == "NSEQ")
                        {
                            iEvalue = iEvalue / int.Parse(m.Groups[2].Value);
                            int useLength = contigLength < 200 ? contigLength : 200;
                            if (genomeLength < 1000)
                            {
                                iEvalue = iEvalue * genomeLength / useLength;
                            }
                            else
                            {
                                iEvalue = iEvalue * 1000 / useLength;
                                iEvalue = iEvalue * (genomeLength - 370.48) / (1000 - 370.48);
                            }
                        }
                    }
                    if (iEvalue < 0)
                    {
                        throw new Exception("Evalue is less than zero");
                    }
                    if (iEvalue < Max_iEValue)
                    {


                        targetName = $"{genome}|{contigName}";

                        AddDomain(GeneBased, Interest, Large, out tChrom, Query, targetName, iEvalue, out ContigUnique, GeneName, Start, Stop, qStart, Strand, parr);
                    }
                }
                SR.Close(); 
            }
        }
        private static void AddDomain(bool GeneBased, Query_Interest Interest, Dictionary<string, Domain_Track_ForChromosome> Large, out Domain_Track_ForChromosome tChrom, string Query, string Target, double iEvalue, out string ContigUnique, string GeneName, int Start, int Stop, int qStart, bool Strand, string[] parr)
        {
            ContigUnique = Target;
            if (!Large.ContainsKey(ContigUnique)) Large.Add(ContigUnique, new Domain_Track_ForChromosome(ContigUnique));
            tChrom = Large[ContigUnique];
            tChrom.Add_Std(Query, iEvalue, Start, Stop, qStart, Strand, GeneName, Interest);
        }

        private static string LocalLocation = @"C:\Temp\";

        /// <summary>
        /// This checks to see if the file is stored locally, and if not it copies it to a local location and returns that location instead
        /// </summary>
        public static string CopyToLocal(string path)
        {
            if (!path.StartsWith("\\\\")) return path; //Already local
            string LocalPath = Path.Combine(LocalLocation, Path.GetFileName(path));
            FileInfo FI = new FileInfo(LocalPath);
            if (!FI.Exists)
            {
                File.Copy(path, FI.FullName);
            }
            return FI.FullName;
        }

        public static void RemoveOverlapping(List<Domain_Track_ForChromosome> list)
        {
            foreach (Domain_Track_ForChromosome item in list)
            {
                item.RemoveOverlapping();
            }
        }
        public static string KeyMaker(string v1)
        {
            return v1.ToUpper().Trim();
        }

        private static Dictionary<string, Dictionary<string, int>> PerMonth_GenomeOrdering = new Dictionary<string, Dictionary<string, int>>();

        private static void DG_AddItem(List<double> list, List<string> arr)
        {
            if (!PerMonth_GenomeOrdering.ContainsKey(arr[5])) PerMonth_GenomeOrdering.Add(arr[5], new Dictionary<string, int>());
            if (!PerMonth_GenomeOrdering[arr[5]].ContainsKey(arr[4])) PerMonth_GenomeOrdering[arr[5]].Add(arr[4], PerMonth_GenomeOrdering[arr[5]].Count);
            int idx = PerMonth_GenomeOrdering[arr[5]][arr[4]];
            AddOut(list, idx, double.Parse(arr[3]));
        }

        private static void AddOut(List<double> list, int Index, double Value)
        {
            while (list.Count <= Index) list.Add(0);
            list[Index] = Value > 0 ? 1 : 0;
        }

    }
}