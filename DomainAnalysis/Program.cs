﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainAnalysis
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Please provide a root folder");
                return;
            }
            string rootFolder = args[0];
            List<string> PathCompileds = (new DirectoryInfo(rootFolder)).GetFiles()
                                                                     .Where(f => f.Name.EndsWith("txt"))
                                                                     .Where(f => f.Name.StartsWith("output"))
                                                                     .Where(f => !Path.GetFileNameWithoutExtension(f.Name).EndsWith("Trimmed"))
                                                                     .Where(f => !Path.GetFileNameWithoutExtension(f.Name).EndsWith("DomainSpacing"))
                                                                     .OrderByDescending(f => f.LastWriteTime)
                                                                     .Select(f => f.FullName)
                                                                     .ToList();

            Dictionary<string, int> DictLength = new Dictionary<string, int>();
            string lengthFile = Path.Combine(rootFolder, "genomelengths.txt");
            using (var lengthReader = new StreamReader(lengthFile))
            {
                lengthReader.ReadLine();
                string line;
                string[] splits;
                while ((line = lengthReader.ReadLine()) != null)
                {
                    splits = line.Split('\t');
                    DictLength.Add(splits[0].ToUpper(), int.Parse(splits[1]));
                }
            }
            foreach (var f in PathCompileds)
            {
                Console.WriteLine(Path.GetFileName(f));
                HMM_Helper.Distances(rootFolder, DictLength, f);
            }

            Console.WriteLine("Finished with trimming and spacing");

            string ResultDirectory = Path.Combine(rootFolder, "finalresults");
            Directory.CreateDirectory(ResultDirectory);
            List<string> TrimmedFiles = (new DirectoryInfo(rootFolder)).GetFiles()
                                                                       .Where(f => f.Name.EndsWith("txt"))
                                                                       .Where(f => f.Name.StartsWith("output"))
                                                                       .Where(f => Path.GetFileNameWithoutExtension(f.Name).EndsWith("Trimmed"))
                                                                       .Select(f => f.FullName)
                                                                       .ToList();
            List<string> SpacingFiles = (new DirectoryInfo(rootFolder)).GetFiles()
                                                           .Where(f => f.Name.EndsWith("txt"))
                                                           .Where(f => f.Name.StartsWith("output"))
                                                           .Where(f => Path.GetFileNameWithoutExtension(f.Name).EndsWith("DomainSpacing"))
                                                           .Select(f => f.FullName)
                                                           .ToList();

            Console.WriteLine("Compiling now...");
            FinalCompile(TrimmedFiles, Path.Combine(ResultDirectory, $"CombinedTrims.txt"));
            FinalCompile(SpacingFiles, Path.Combine(ResultDirectory, $"CombinedSpacing.txt"));

        }

        static void FinalCompile(List<string> files, string outfilepath)
        {
            bool headerwritten = false;
            using (StreamWriter writer = File.AppendText(outfilepath))
            {
                string header;
                string line;
                foreach (string f in files)
                {
                    using (var reader = new StreamReader(f))
                    {
                        header = reader.ReadLine();
                        if (!headerwritten)
                        {
                            writer.WriteLine(header);
                            headerwritten = true;
                        }
                        while ((line = reader.ReadLine()) != null)
                        {
                            writer.WriteLine(line);
                        }
                    }
                }
            }
        }
    }
}
