﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace GEiCToolsApp
{
    namespace DomainCentric
    {
        public class HMM_Out_Work
        {
            private static string[] splitTab = new string[1] { "\t" };
            private static string[] splitSpace = new string[1] { " " };

            public static void ParseOutFiles(string FolderPath, List<FileInfo> Files)
            {
                bool OnlyCheckCompletion = false;
                

                System.Text.RegularExpressions.Regex Regexp = new System.Text.RegularExpressions.Regex(@"_F(\d)_(\d+)"); // This is important for windows and offsets
                System.Text.RegularExpressions.Match Match;
                int WindowShift = 0; string tWS; int FromOrTo;
                List<string> tList; List<string> tHeader = null;
                List<List<string>> Results = new List<List<string>>();
                List<int> PositionsKeep = new List<int>();

                int ExportStyle = 1;
                
                if (ExportStyle == 0) PositionsKeep = new List<int>(8) { 0, 3, 4, 12, 15, 16, 19, 20 };
                if (ExportStyle == 1) PositionsKeep = new List<int>(12) { 0, 3, 4, 11, 12, 13, 15, 16, 19, 20 };

                StreamWriter SW = null;
                if (OnlyCheckCompletion)
                {
                    File.WriteAllLines(@"C:\temp\AllNamesIn_4HMM.txt", Files.Select(e => e.Name));
                }
                else
                {
                    Debug.Print("Starting new file . . ");
                    SW = new StreamWriter(Path.Combine((new DirectoryInfo(FolderPath)).Parent.FullName, $"output_new_{DateTime.Now.ToString("yyyyMMdd hhmm")}.txt"));
                    
                    if (ExportStyle == 0) SW.WriteLine("File\tTarget\tQueryName\tQueryAcc\tiEvalue\tQfrom\tQto\tTfrom\tTto\tFrame");

                    if (ExportStyle == 1) SW.WriteLine("File\tTarget\tQueryName\tQueryAcc\tcEvalue\tiEvalue\tScore\tQfrom\tQto\tTfrom\tTto\tFrame");
                }
                StringBuilder sB = new StringBuilder(); string t; int Counter = 0; string frame;
                string OpenPath;
                foreach (FileInfo FI in Files)
                {
                    OpenPath = FI.FullName;
                    if (OnlyCheckCompletion)
                    {
                        t = File.ReadLines(OpenPath).Last();
                        if (t != "# [ok]")
                            sB.Append(FI.Name);
                    }
                    else
                    { 
                        using (StreamReader SR = new StreamReader(OpenPath)) {
                            SR.ReadLine();
                            while (!SR.EndOfStream)
                            {
                                t = SR.ReadLine();
                                if (t.StartsWith("# target") && tHeader == null) // The line with the header information
                                {
                                    tHeader = t.Split(splitSpace, StringSplitOptions.RemoveEmptyEntries).ToList();
                                }
                                else if (!t.StartsWith("#") && (t != "")) // Lines with domains
                                {
                                    tList = t.Split(splitSpace, StringSplitOptions.RemoveEmptyEntries).ToList();
                                    Match = null;
                                    sB = new StringBuilder();
                                    sB.Append(FI.Name + "\t");

                                    // Fix up the header name and pull out the frame if necessary
                                    frame = ""; WindowShift = 0;
                                    if (Regexp.IsMatch(tList[0].ToUpper()))
                                    {
                                        // This is specifically for Windows FAA files
                                        Match = Regexp.Match(tList[0].ToUpper());
                                        frame = Match.Groups[1].Value;
                                        tWS = Match.Groups[2].Value;
                                        WindowShift = 0; int.TryParse(tWS, out WindowShift);

                                        //
                                        tList[0] = Regexp.Replace(tList[0].ToUpper(), "");
                                    }
                                    else if (tList[0].Contains("_f"))
                                    {
                                        frame = tList[0].Substring(tList[0].Length - 1, 1);
                                        if (tList[0].EndsWith("_f" + frame))
                                        {
                                            tList[0] = tList[0].Substring(0, tList[0].Length - 3);
                                        }
                                        else
                                        {
                                            frame = "";
                                        }
                                    }
                                    for (int i = 0; i < PositionsKeep.Count; i++)
                                    {
                                        if ((WindowShift != 0) && (tHeader[PositionsKeep[i]] == "from" || tHeader[PositionsKeep[i]] == "to"))
                                        {
                                            // Apply the window shift as needed
                                            FromOrTo = 0;
                                            tWS = tList[PositionsKeep[i]]; int.TryParse(tWS, out FromOrTo);
                                            FromOrTo += WindowShift;
                                            tList[PositionsKeep[i]] = FromOrTo.ToString();
                                        }
                                        sB.Append(tList[PositionsKeep[i]] + "\t");
                                    }
                                    // Now the frame
                                    if (tList.Last() == "-")
                                    {
                                        sB.Append(frame);
                                    }
                                    else
                                    {
                                        sB.Append(tList.Last().Substring(tList.Last().Length - 1));
                                    }
                                    SW.WriteLine(sB.ToString());
                                }
                            }
                            SR.Close();
                        }
                    }
                    
                    if (++Counter % 1000 == 0)
                    {
                        if (!OnlyCheckCompletion) SW.Flush();
                        Debug.Print((100 * (double)Counter / Files.Count).ToString("00.00") + "% " + Counter.ToString());
                    }
                }
                SW.Close();
            }
        }
    }
}
