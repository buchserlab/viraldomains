﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEiCToolsApp
{
    namespace DomainCentric
    {
        class Program
        {
            static void Main(string[] args)
            {
                if (args.Length == 0)
                {
                    Console.WriteLine("Please provide a root folder");
                    return;
                }
                string rootFolder = args[0];
                string FolderPath = Path.Combine(rootFolder, "HMM");
                List<FileInfo> Files;
                List<FileInfo> FileRange;
                int outRange = 100000;
                Files = Directory.EnumerateFiles(FolderPath, "*.out", SearchOption.AllDirectories).Select(s => new FileInfo(s)).ToList();
                for (int j = 0; j < Files.Count; j += outRange)
                {
                    Console.WriteLine($"Starting with file {j} out of {Files.Count}");
                    FileRange = Files.GetRange(j, Math.Min(outRange, Files.Count - j));
                    HMM_Out_Work.ParseOutFiles(FolderPath, FileRange);
                }
            }
        }
    }
}