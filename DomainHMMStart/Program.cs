﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CHPC_Lib;

namespace GEiCToolsApp
{
    namespace DomainCentric
    {
        class Program
        {
            static void Main(string[] args)
            {
                // Start here - - - - - - - - 

                // This needs to be adjusted for each computer
                if (args.Length == 0)
                {
                    Console.WriteLine("Please provide a root folder");
                    return;
                }
                string rootFolder = args[0];

                string ClusterLoginFile = Path.Combine(rootFolder, "cluster_login.txt");
                string ClusterSettingsFile = Path.Combine(rootFolder, "cluster.xml");
                
                ClusterSpecific cs = new ClusterSpecific(ClusterLoginFile, ClusterSettingsFile);
                
                // The code that starts with "ClusterSpecific", can be coded for the particular computing cluster environment you are using
                // All obvious settings for the cluster environment should be able to be changed with the .xml file, but you may have to make tweaks
                // Make sure to change the folder names (below), to match your data's location
                // Below will check your folder with the prepped sequences and give you the scripts to run HMM
                Auto_HMM.Run_HMM(cs, rootFolder, null);
            }
        }

        public static class Auto_HMM
        {
            public static string Folder_Root = @"C:/Temp";
            public static string Folder_RemoteFAA; //Shouldn't have spaces
            public static string Folder_RemoteOut;  //Shouldn't have spaces
            public static string Leaf_3C_FAA = "3C_FAA";
            public static string Leaf_Track = "xx Track";
            public static string Leaf_Outs = "HMM";
            public static double OptimalWallTimeHours = 3;
            public static int PFAMStep = 40; //How many PFAMs to do on each file before continuing. If this is too small, small FAA files will finish too quickly
            public static int FileStep = 15;
            
            public static bool DeleteLocalAfterUpload = false;
            
            public static List<int> PFAM_All = Enumerable.Range(0, 40).ToList();

            public static int ProcessorsPerNode = 4;
            public static System.ComponentModel.BackgroundWorker _BW;

            public static void Run_HMM(ClusterSpecific cs, string RootFolder, System.ComponentModel.BackgroundWorker BW)
            {
                Folder_Root = RootFolder;
                Directory.CreateDirectory(Path.Combine(Folder_Root, Leaf_Track));
                Directory.CreateDirectory(Path.Combine(Folder_Root, Leaf_Outs));
                bool CycleTransferAndCleanupOnFinish = true;
                _BW = BW;
                string FAA_NameMustContain = String.Empty; //Leave Blank to turn this off
                // Start by cleaning up what was done previously
                BWReportProgress(0, "Cleaning previous ..");
                RefreshAll_Delete(cs);
                // Or Clean
                CleanUp_HMM(cs);
                BWReportProgress(0, "Search 3C FAA ..");

                List<long> FileSizes = new List<long>();
                // Look for new 3C FAA Files that HAVEN'T BEEN WORKED ON
                List<string> AllFAAFiles = Search_3CFAAs();
                AllFAAFiles.Reverse();
                FileSizes.Reverse();
                // Loop through those files
                int ScriptsStarted; int Total_ScriptsStarted;
                List<string> PFAM_Rel1 = PFAM_All.ConvertAll(x => x.ToString());
                List<string> PFAM_Ordered = new List<string>(PFAM_Rel1);
                List<string> PFAM_Use = PFAM_Ordered;

                // Alters the file ending of the pfam files
                PFAM_Use = PFAM_Use.Select(s => s.EndsWith(".hmm") ? Path.GetFileNameWithoutExtension(s) : s).ToList();

                List<string> PFAMIter;
                List<FileInfo> faaIter;
                List<string> filenameIter;
                List<string> LocalFiles;
                List<string> RemoteFiles;


                Total_ScriptsStarted = 0;
                for (int iFile = 0; iFile < AllFAAFiles.Count; iFile += FileStep)
                {
                    bool FirstTimeUpload = true;
                    filenameIter = AllFAAFiles.GetRange(iFile, Math.Min(AllFAAFiles.Count - iFile, FileStep));
                    foreach (var name in filenameIter) BWReportProgress(0, $"Starting {name}");
                    for (int iPFAM = 0; iPFAM < PFAM_Use.Count; iPFAM += PFAMStep)
                    {
                        PFAMIter = PFAM_Use.GetRange(iPFAM, Math.Min(PFAM_Use.Count - iPFAM, PFAMStep));
                        if (BW != null && BW.CancellationPending)
                        {
                            BWReportProgress(0, "CANCELLED");
                            return;
                        }
                        PauseCheckQueue(cs);
                        faaIter = filenameIter.Select(f => new FileInfo(f)).ToList();
                        // Checks whether the FAA_NameMustContain exists and if the FAA file contains the necessary string; if not the FAA file is skipped 
                        if (FAA_NameMustContain != String.Empty)
                        {
                            faaIter = faaIter.Where(f => f.Name.Contains(FAA_NameMustContain)).ToList();
                            if (faaIter.Count == 0) continue;
                        }
                        LocalFiles = faaIter.Select(f => f.FullName).ToList();
                        RemoteFiles = faaIter.Select(f => ClusterSpecific.PathCombineUnix(cs.RemoteFAAFolder, f.Name)).ToList();
                        BWReportProgress(0, "Uploading Files...");
                        bool uploadSuccess = UploadFAAs(cs, LocalFiles, RemoteFiles, FirstTimeUpload);
                        if (!uploadSuccess) Debugger.Break();
                        foreach (var FI in faaIter) BWReportProgress(0, "  " + FI.Name);

                        BWReportProgress(0, "  Create/Upload/Start PFAMS (" + ((double)Math.Min(iPFAM + PFAMStep, PFAM_Use.Count) / PFAM_Use.Count).ToString("0.0%") + ")");
                        ScriptsStarted = StartHMMScripts(cs, faaIter, RemoteFiles, PFAMIter);
                        
                        if (DeleteLocalAfterUpload)
                        {
                            foreach (FileInfo FI in faaIter) FI.Delete();
                        }
                        
                        if (ScriptsStarted > 1) CleanUp_HMM(cs); // Check what has already finished to keep things cleaned up
                        Total_ScriptsStarted += ScriptsStarted;
                        FirstTimeUpload = false;
                    }
                    CleanUp_HMM(cs);
                }
                BWReportProgress(0, "Finished!");
                int FinishedCount = 0;
                while (CycleTransferAndCleanupOnFinish)
                {
                    int CleanedUp = 1;
                    int.TryParse(CleanUp_HMM(cs), out CleanedUp);
                    BWReportProgress(0, $"Cycling for Cleaning {CleanedUp}");
                    PauseCheckQueue(cs);

                    if (CleanedUp < 1) FinishedCount++;
                    else FinishedCount = 0;

                    if (FinishedCount >= 2) break; // Stops after two times in a row where there are no jobs left
                    for (int j = 0; j < 60 * 15; j++) // 15 minutes between checks
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                }
            }

            private static List<string> Search_3CFAAs_Remote(ClusterSpecific cs, out List<long> fileSizes)
            {
                string fileCommand = $"for entry in {cs.RemoteFAAFolder}/*; do printf \"$entry \"; done";
                string sizeCommand = $"for entry in {cs.RemoteFAAFolder}/*; do printf \"$(stat -c%s \"$entry\") \"; done";
                string remoteFileNames = cs.RunCommand(fileCommand, false);
                string remoteFileSizes = cs.RunCommand(sizeCommand, false);
                fileSizes = remoteFileSizes.Trim().Split(' ').Select(s => long.Parse(s)).ToList();
                return remoteFileNames.Trim().Split(' ').ToList();
            }

            /// <summary>
            /// Checks whether the maximum number of jobs has been reached (2500). If so, waits in cycles of five minutes 
            /// </summary>
            private static void PauseCheckQueue(ClusterSpecific cs)
            {
                int MaxJobs = 250;
                int CurrentJobs = cs.JobsRunning();
                Debug.Print("Jobs Running: " + CurrentJobs);
                while (CurrentJobs > MaxJobs)
                {
                    BWReportProgress(0, $"There are currently {CurrentJobs} running");
                    for (int j = 0; j < 60 * 5; j++) //5 minute wait
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    CurrentJobs = cs.JobsRunning();
                }
            }

            private static void BWReportProgress(int v1, string Message)
            {
                if (_BW != null) _BW.ReportProgress(v1, Message);
                else Console.WriteLine(Message);
                Debug.Print(Message);
            }

            /// <summary>
            /// Uploads the FAA files to the remote server
            /// </summary>
            /// <param name="cs">The clusterspecific object corresponding to the server</param>
            /// <param name="LocalFiles">A List containing the full local FAA file names</param>
            /// <param name="RemoteFiles">A List containing the full remote FAA file names</param>
            /// <param name="CheckForUpload">Whether or not to actually check for the upload</param>
            /// <returns>true if the upload worked</returns>
            public static bool UploadFAAs(ClusterSpecific cs, List<string> LocalFiles, List<string> RemoteFiles, bool CheckForUpload)
            {
                if (!CheckForUpload) return true;
                if (LocalFiles.Count != RemoteFiles.Count) throw new ArgumentException("LocalFiles and RemoteFiles are not the same length");

                List<string> ToUploadLocal = new List<string>();
                List<string> ToUploadRemote = new List<string>();

                string track;
                for (int i = 0; i < LocalFiles.Count; i++)
                {
                    track = Track_GetUpdate(Path.GetFileName(LocalFiles[i]));
                    if (track == null) track = String.Empty;
                    
                    if (!track.Contains($"Uploaded{cs.Name}"))
                    {
                        ToUploadLocal.Add(LocalFiles[i]);
                        ToUploadRemote.Add(RemoteFiles[i]);
                    }
                }
                if (cs.UploadFiles(ToUploadRemote, ToUploadLocal))
                {
                    // Track that it was successfully uploaded
                    foreach (string LocalFAA in ToUploadLocal) TrackSetUpdate(Path.GetFileName(LocalFAA), $"Uploaded{cs.Name}");
                }
                else
                {
                    return false; //ERROR UPLOADING
                }
                return true;
            }

            private static int StartHMMScripts(ClusterSpecific cs, List<FileInfo> LocalFiles, List<string> RemoteFAAFilenames, List<string> HMMsToRun)
            {
                return StartHMMScripts(cs, LocalFiles.Select(f => f.Length).ToList(), RemoteFAAFilenames, HMMsToRun);
            }

            public static int StartHMMScripts(ClusterSpecific cs, List<long> FileSizes, List<string> RemoteFAAFilenames, List<string> HMMsToRun)
            {
                if (FileSizes.Count != RemoteFAAFilenames.Count) throw new ArgumentException("File list lengths do not match");
                ClusterHelper ch = new ClusterHelper(cs);
                var AllScripts = new List<string>();
                var AllRemoteScriptFiles = new List<string>();
                var AllTracking = new List<string>(FileSizes.Count);

                string tracking = "";

                for (int i = 0; i < FileSizes.Count; i++)
                {
                    HashSet<string> AlreadyFinished = Track_GetUpdatesHMMs(Path.GetFileName(RemoteFAAFilenames[i]));
                    List<string> HMMsToRunNew = HMMsToRun.Except(AlreadyFinished).ToList();
                    if (HMMsToRunNew.Count <= 0)
                    {
                        AllTracking.Add(String.Empty);
                        continue;
                    }
                    (List<string> Scripts, List<string> RemoteScriptFiles) = GenerateScripts(ch, FileSizes[i], RemoteFAAFilenames[i], HMMsToRunNew, out tracking);
                    AllScripts.AddRange(Scripts);
                    AllRemoteScriptFiles.AddRange(RemoteScriptFiles);
                    AllTracking.Add(tracking);
                }

                // Upload all the scripts
                if (AllRemoteScriptFiles.Count == 0)
                {
                    BWReportProgress(0, "Already Done!");
                    return 0;
                }

                if (!(AllScripts.Count == AllRemoteScriptFiles.Count))
                {
                    Debugger.Break();
                }
                cs.CreateTextFiles(AllScripts, AllRemoteScriptFiles);

                // Start running the scripts
                List<string> Track = cs.StartBatchScripts(AllRemoteScriptFiles);

                for (int i = 0; i < FileSizes.Count; i++)
                {
                    TrackSetUpdate(Path.GetFileName(RemoteFAAFilenames[i]), AllTracking[i]);
                }
                return AllRemoteScriptFiles.Count;
            }

            public static (List<string> Scripts, List<string> RemoteScriptFiles) GenerateScripts(ClusterHelper ch, long fileSize, string remoteFAAFilename, List<string> HMMsToRun, out string Tracking)
            {
                if (fileSize == 0)
                {
                    Tracking = String.Empty;
                    return (new List<string>(), new List<string>());
                }
                double FAA_Gigabytes = (double)fileSize / 1024 / 1024 / 1024;
                double PreSearches = OptimalWallTimeHours / (FAA_Gigabytes / 8);
                int SearchesPerNode = (int)Math.Max(1, Math.Floor(PreSearches));
                double ActualWallTime = Math.Max(0.5, PreSearches >= 1 ? OptimalWallTimeHours : 1 / PreSearches);

                //First generate the id files and scripts
                List<string> RemoteScriptFiles = new List<string>();
                List<string> Scripts = new List<string>();
                string tRemoteLocation;
                int Count;


                List<string> HMMRange;
                string script;

                for (int iHMM = 0; iHMM < HMMsToRun.Count; iHMM += SearchesPerNode)
                {
                    Count = Math.Min(SearchesPerNode, HMMsToRun.Count - iHMM);
                    HMMRange = HMMsToRun.GetRange(iHMM, Count);
                    script = ch.Build_HMMScript_DC(
                        remoteFAAFilename, Path.GetFileName(remoteFAAFilename), HMMRange,
                        ProcessorsPerNode, ActualWallTime, Folder_RemoteOut,
                        out tRemoteLocation);
                    Scripts.Add(script);
                    RemoteScriptFiles.Add(tRemoteLocation);
                }

                // Save which ones have been started
                Tracking = String.Join("|", HMMsToRun) + "|";

                return (Scripts, RemoteScriptFiles);
            }

            /// <summary>
            /// Adds an update to a tracking file for a given FAA 
            /// </summary>
            /// <param name="FAAName">The name of the FAA file</param>
            /// <param name="Update">The text to be added to the tracking file</param>
            public static void TrackSetUpdate(string FAAName, string Update)
            {
                if (Update == String.Empty) return;
                //Search for file
                FileInfo Track = new FileInfo(Path.Combine(Folder_Root, Leaf_Track, FAAName));
                StreamWriter SW;
                if (!Track.Exists)
                {
                    SW = Track.AppendText();
                    SW.WriteLine(FAAName + "\t" + DateTime.Now + "\r\n");
                }
                else
                {
                    SW = Track.AppendText();
                }

                SW.WriteLine(Update);
                SW.Close();
            }

            private static string[] splitPipe = new string[1] { "|" };
            private static string[] splitAll = new string[3] { "|", "\r", "\n" };

            public static HashSet<string> Track_GetUpdatesHMMs(string FullPathFAA)
            {
                string s = Track_GetUpdate(Path.GetFileName(FullPathFAA));
                if (s == null) return new HashSet<string>();
                string[] arr = s.Split(splitAll, StringSplitOptions.RemoveEmptyEntries);

                return new HashSet<string>(arr);
            }

            public static List<string> Track_GetUpdates(string FullPathFAA)
            {
                //Search for file
                string FAAName = Path.GetFileName(FullPathFAA);
                FileInfo Track = new FileInfo(Path.Combine(Folder_Root, Leaf_Track, FAAName));
                if (!Track.Exists)
                {
                    return null;
                }
                else
                {
                    string[] Lines = File.ReadAllLines(Track.FullName);
                    return Lines.ToList();
                }
            }

            /// <summary>
            /// Gets the tracking information for a given FAA file
            /// </summary>
            /// <param name="FullPathFAA"> The full path of the FAA file</param>
            /// <returns>The contents of the tracking file, if it exists</returns>
            public static string Track_GetUpdate(string FAAName)
            {
                //Search for file
                FileInfo Track = new FileInfo(Path.Combine(Folder_Root, Leaf_Track, FAAName));
                if (!Track.Exists)
                {
                    return null;
                }
                else
                {
                    return File.ReadAllText(Track.FullName);
                }
            }

            public static List<string> Search_3CFAAs(string FullPath)
            {
                // Returns the total number of files found, and the list that haven't been worked on with their sizes
                DirectoryInfo DI_FAA = new DirectoryInfo(FullPath);
                return DI_FAA.GetFiles().Select(f => f.FullName).ToList();
            }

            public static List<string> Search_3CFAAs()
            {
                // Returns the total number of files found, and the list that haven't been worked on with their sizes
                return Search_3CFAAs(Path.Combine(Folder_Root, Leaf_3C_FAA));
            }

            public static void RefreshAll_Delete(ClusterSpecific cs)
            {
                // Refresh all
                // WARNING, THIS WILL DELETE EXISTING SCRIPTS AND UPLOADED FAA FILES
                bool DeleteWorked = cs.Refresh_Delete();
            }

            public static string CleanUp_HMM(ClusterSpecific cs)
            {
                // Delete the DC_ o, and e file from the home folder
                bool DeleteWorked = cs.DeleteFiles_StartsWith("DC_");

                string MoveWorked = cs.MoveCompletedHMM();
                bool CopyLocalWorked = cs.CopyToLocalCompletedHMM(Path.Combine(Folder_Root, Leaf_Outs));

                bool DeleteMoved = cs.DeleteMoved();
                
                return MoveWorked;
            }
        }

        public class ClusterHelper
        {
            public string RemotePath_Home;
            public string RemotePath_Scratch;
            public string RemotePath_Scratch_Temp;
            public string BatchSystem;

            public static string PathCombineUnix(string Path1, string Path2)
            {
                if (!Path1.EndsWith("/")) Path1 += "/";
                if (Path2.StartsWith("/"))
                    return Path1 + Path2.Substring(1);
                else
                    return Path1 + Path2;
            }

            public ClusterHelper(ClusterSpecific cs)
            {
                RemotePath_Home = cs.RemotePath_Home;
                RemotePath_Scratch = cs.RemotePath_Scratch;
                BatchSystem = cs.BatchSystem;
                RemotePath_Scratch_Temp = cs.RemoteHMMTempFolder;
            }

            public string Build_HMMScript_DC(string FAA_Path, string FAA_Short, List<string> HMMs, int ProcessorsPerNode, double WallTime_Hours, string RemoteOutPath, out string RemotePath)
            {
                string tName = FAA_Short.Replace(".", "").Replace("_", "").Replace("-", "") + "_" + HMMs[0];

                string ScriptFileName = "scriDCp_" + tName;

                string script = WriteHMMBatchScript(
                    "DC_" + tName.Substring(0, Math.Min(tName.Length, 15)),
                    FAA_Path, FAA_Short,
                    HMMs,
                    RemoteOutPath,
                    BatchSystem, ProcessorsPerNode, WallTime_Hours);

                RemotePath = PathCombineUnix(RemotePath_Home, ScriptFileName);
                return script;
            }

            public string WriteHMMBatchScript(string Job_Name, string FAA_Full, string FAA_Short, List<string> HMMs, string RemoteOutPath, string BatchSystem, int ProcessorsPerNode, double WallTime_Hours)
            {
                string HMM;
                int hours = (int)Math.Truncate(WallTime_Hours);
                int minutes = (int)((WallTime_Hours - hours) * 60);
                string timeString = $"{hours.ToString()}:{minutes.ToString("00")}:00";
                StringBuilder script = new StringBuilder();
                script.Append("#!/bin/bash -l\n");

                if (BatchSystem == "PBS") {
                    script.Append($"#PBS -l nodes=1:ppn={ProcessorsPerNode},walltime={timeString}\n");
                    script.Append($"#PBS -N {Job_Name}\n");
                }
                else if (BatchSystem == "Slurm")
                {
                    script.Append($"#SBATCH -J {Job_Name}\n");
                    script.Append( "#SBATCH -N 1\n");
                    script.Append($"#SBATCH --ntasks-per-node {ProcessorsPerNode}\n");
                    script.Append("module load openmpi/3.1.3-python-2.7.15 hmmer/3.2.1-python-2.7.15\n");
                }
                else
                {
                    throw new Exception("Couldn't recognize computing cluster batch system");
                }
                
                for (int i = 0; i < HMMs.Count; i++)
                {
                    HMM = HMMs[i];
                    // We changed the threshold on hmmsearch to a minimum bit score of -5 (very generous) because the automatic threshold is based off of the
                    // conditional e-value, which changes based on which windows are hits, so regions with more domains will underreport all their matches
                    script.Append($"hmmsearch --noali --domT -5 -o /dev/null --domtblout {RemotePath_Scratch_Temp}/d_{Path.GetFileNameWithoutExtension(FAA_Short)}[hmm{HMM}].out {RemotePath_Home}/pfam/{HMM}.hmm {FAA_Full}\n");
                }
                script.Append("exit 0\n");
                
                return script.ToString();
            }
        }

    }
}