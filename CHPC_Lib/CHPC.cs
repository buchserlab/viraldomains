﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Renci.SshNet;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json;
using System.Xml;

namespace CHPC_Lib
{
    
    public class ClusterSpecific
    {
        public readonly string Name;
        public string hostname;
        public string username;
        private readonly string password;

        public string BatchSystem;

        public string RemotePath_Home;
        public string RemotePath_Scratch;

        public string RemoteFAAFolder     => PathCombineUnix(RemotePath_Scratch, "3C_FAA");
        public string RemoteHMMTempFolder => PathCombineUnix(RemotePath_Scratch, "4_HMM");
        public string RemoteHMMDoneFolder => PathCombineUnix(RemotePath_Scratch, "4OK");
        
        public static string PathCombineUnix(string Path1, string Path2)
        {
            if (!Path1.EndsWith("/")) Path1 += "/";
            if (Path2.StartsWith("/"))
                return Path1 + Path2.Substring(1);
            else
                return Path1 + Path2;
        }

        public ClusterSpecific(string LoginPath, string SettingsPath)
        {
            Name = Path.GetFileNameWithoutExtension(SettingsPath).ToUpper();
            try
            {
                using (StreamReader sr = new StreamReader(LoginPath))
                {
                    username = sr.ReadLine();
                    password = sr.ReadLine();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Login file does not exist");
                Console.WriteLine(e.Message);
            }
            LoadSettings(SettingsPath);
        }

        /// <summary>
        /// A method to load settings from an xml file. This is written as a separate function for debugging purposes
        /// </summary>
        /// <param name="SettingsPath">The path of the settings xml file</param>
        public void LoadSettings(string SettingsPath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(SettingsPath);
            hostname           = doc.DocumentElement.SelectSingleNode("/Settings/hostname").InnerText;
            BatchSystem        = doc.DocumentElement.SelectSingleNode("/Settings/BatchSystem").InnerText;
            RemotePath_Home    = doc.DocumentElement.SelectSingleNode("/Settings/RemotePathHome").InnerText;
            RemotePath_Scratch = doc.DocumentElement.SelectSingleNode("/Settings/RemotePathScratch").InnerText;
        }


        public bool DeleteFolder(string RemoteFolder)
        {
            bool Success = false;
            using (SshClient client = new SshClient(hostname, username, password))
            {
                SshCommand c;
                client.Connect();
                c = client.RunCommand("rm -r " + RemoteFolder);
                if (c.Error != "")
                {
                    Success = true;
                }

                client.Disconnect();
            }
            return Success;
        }

        public List<string> StartBatchScripts(List<string> ScriptNames)
        {
            List<string> Track = new List<string>();
            using (SshClient client = new SshClient(hostname, username, password))
            {
                SshCommand c;
                client.Connect();
                for (int i = 0; i < ScriptNames.Count; i++)
                {
                    if (BatchSystem == "PBS")
                    {
                        c = client.RunCommand($"qsub {ScriptNames[i]}");
                    }
                    else if (BatchSystem == "Slurm")
                    {
                        c = client.RunCommand($"sbatch -o /dev/null --workdir {RemotePath_Scratch} {ScriptNames[i]}");
                        //Console.WriteLine(c.Result);
                    }
                    else
                    {
                        throw new Exception("Your batch system could not be located");
                    }
                    Track.Add(c.Result);
                    if (ScriptNames.Count > 10)
                    {
                        System.Threading.Thread.Sleep(1500);
                        if (i % 200 == 0) Debug.Print($"Starting {i}");
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(150);
                    }
                }
                client.Disconnect();
            }
            return Track;
        }


        #region Upload and Download

        private static string[] splitLineBreak = new string[2] { "\r", "\n" };

        // Fix to check if empty first
        public bool Refresh_Delete()
        {
            bool finished = false;
            var toRemove = new (string folder, string pattern)[] {
                ($"{RemoteHMMDoneFolder}/", "*.out"),
                ($"{RemotePath_Home}/", "scriDCP*")
            };
            StringBuilder sB = new StringBuilder();
            while (!finished)
            {
                try
                {
                    using (SshClient client = new SshClient(hostname, username, password))
                    {
                        SshCommand c;
                        client.Connect();
                        foreach ((string folder, string pattern) in toRemove)
                        {
                            sB.Append($"find {folder} -maxdepth 1 -name '{pattern}' -type f -delete\n");
                        }
                        c = client.RunCommand(sB.ToString());
                        if (c.Error != "")
                        {
                            Debugger.Break();
                            return false;
                        }
                        client.Disconnect();
                        finished = true;
                    }
                }
                catch
                {
                    System.Threading.Thread.Sleep(5000);
                }
            }
            return true;
        }

        public bool DeleteFiles_StartsWith(string StartsWith)
        {
            bool finished = false;
            while (!finished)
            {
                try
                {
                    using (SshClient client = new SshClient(hostname, username, password))
                    {
                        SshCommand c;
                        client.Connect();

                        // The double brackets are just so that the string interpolation reads single brackets
                        c = client.RunCommand($"find . -type f -name \"{StartsWith}*\" -exec rm {{}} \\;");
                        if (c.Error != "")
                        {
                            Debugger.Break();
                            return false;
                        }
                        client.Disconnect();
                        finished = true;
                    }
                }
                catch
                {
                    System.Threading.Thread.Sleep(5000);
                }
            }
            return true;
        }

        public bool UploadFile(string RemoteFilePath, string LocalFilePath)
        {
            if (BatchSystem == "PBS")
            {
                try
                {
                    // https://searchcode.com/codesearch/view/10948381/
                    using (var scp = new ScpClient(hostname, username, password))
                    {
                        scp.Connect();
                        FileInfo FI_Up = new FileInfo(LocalFilePath);
                        scp.Upload(FI_Up, RemoteFilePath);

                        scp.Disconnect();
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
                try
                {
                    using (var sftp = new SftpClient(hostname, username, password))
                    {
                        sftp.Connect();
                        FileInfo FI_Up = new FileInfo(LocalFilePath);
                        Stream stream = FI_Up.OpenRead();
                        sftp.UploadFile(stream, RemoteFilePath);
                        stream.Close();
                        sftp.Disconnect();
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
           
        }

        public bool UploadFiles(List<string> RemoteFilePaths, List<string> LocalFilePaths)
        {
            return UploadFiles(RemoteFilePaths, LocalFilePaths, false);
        }

        public bool UploadFiles(List<string> RemoteFilePaths, List<string> LocalFilePaths, bool DeleteLocalAfterUpload)
        {
            if (BatchSystem == "PBS")
            {
                try
                {
                    //https://searchcode.com/codesearch/view/10948381/
                    using (var scp = new ScpClient(hostname, username, password))
                    {
                        scp.Connect();
                        FileInfo FI_Up;
                        for (int i = 0; i < RemoteFilePaths.Count; i++)
                        {
                            if (RemoteFilePaths.Count > 10)
                            {
                                if (i % 100 == 0) { Debug.Print($"Uploaded: {i}"); }
                            }
                            FI_Up = new FileInfo(LocalFilePaths[i]);
                            if (FI_Up.Exists)
                            {
                                scp.Upload(FI_Up, RemoteFilePaths[i]);
                                if (DeleteLocalAfterUpload) FI_Up.Delete();
                            }
                        }
                        scp.Disconnect();
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                try
                {
                    //https://searchcode.com/codesearch/view/10948381/
                    using (var sftp = new SftpClient(hostname, username, password))
                    {
                        sftp.Connect();
                        FileInfo FI_Up;
                        Stream stream;
                        for (int i = 0; i < RemoteFilePaths.Count; i++)
                        {
                            if (RemoteFilePaths.Count > 10)
                            {
                                if (i % 100 == 0) { Debug.Print($"Uploaded: {i}"); }
                            }
                            FI_Up = new FileInfo(LocalFilePaths[i]);
                            if (FI_Up.Exists)
                            {
                                stream = FI_Up.OpenRead();
                                sftp.UploadFile(stream, RemoteFilePaths[i]);
                                stream.Close();
                                if (DeleteLocalAfterUpload) FI_Up.Delete();
                            }
                        }
                        sftp.Disconnect();
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public string MoveCompletedHMM()
        {
            StringBuilder SB = new StringBuilder();

            SB = new StringBuilder();
            SB.Append($"for nfile in {RemoteHMMTempFolder}/*.out; do out041=\"$(tail -c 5 $nfile)\"; if [ $out041 = \"[ok]\" ]; then mv $nfile {RemoteHMMDoneFolder}/; echo moved; fi; done");
            string[] arr = new string[1] { "" }; string[] split = new string[1] { "moved" };
            using (SshClient client = new SshClient(hostname, username, password))
            {
                SshCommand c;
                client.Connect();
                c = client.RunCommand(SB.ToString());
                client.Disconnect();
                // Can also count how many moved there are
                if (c.Result == "")
                {
                    return "-1";
                }
                arr = c.Result.Split(split, StringSplitOptions.RemoveEmptyEntries);
            }
            return arr.Length.ToString();
        }


        public bool CopyToLocalCompletedHMM(string LocalFolder)
        {
            Directory.CreateDirectory(LocalFolder);
            Process P = Process.Start("pscp.exe", $"-pw \"{password}\" {username}@{hostname}:{RemoteHMMDoneFolder}/* {LocalFolder}");
            P.WaitForExit();
            return true;
        }

        public bool DeleteMoved()
        {
            bool finished = false;
            while (!finished)
            {
                try
                {
                    using (SshClient client = new SshClient(hostname, username, password))
                    {
                        SshCommand c;
                        client.Connect();
                        c = client.RunCommand($"find {RemoteHMMDoneFolder} -name '*.out' -delete");
                        client.Disconnect();
                        if (c.Error != "")
                        {
                            return false;
                        }
                    }
                    return true;
                }
                catch
                {
                    System.Threading.Thread.Sleep(5000);
                }
            }
            return true;
        }

        public int JobsRunning()
        {
            string res;
            using (SshClient client = new SshClient(hostname, username, password))
            {
                SshCommand c;
                client.Connect();
                if (BatchSystem == "PBS")
                {
                    c = client.RunCommand($"qstat -u {username} | wc -l");
                }
                else if (BatchSystem == "Slurm")
                {
                    c = client.RunCommand($"squeue -u {username} | wc -l");
                }
                else
                {
                    throw new Exception("Your batch system could not be located");
                }
                client.Disconnect();
                res = c.Result;
                if (c.Error != "")
                {
                    return -1;
                }
            }
            int ret = -1;
            int.TryParse(res, out ret);

            // qstat gives a five-line header if there are any jobs, and squeue always gives a one-line header, so this must be accounted for
            if (BatchSystem == "PBS" && ret > 0)
            {
                ret -= 5;
            }
            else if (BatchSystem == "Slurm")
            {
                ret -= 1;
            }
            return ret;
        }

        public void CreateTextFiles(List<string> Scripts, List<string> RemoteScriptFiles)
        {
            if (Scripts.Count != RemoteScriptFiles.Count) throw new ArgumentException("The Argument lengths do not match");
            List<string> commands = new List<string>(Scripts.Count);
            for (int i = 0; i < Scripts.Count; i++) commands.Add(Create_TextFile_Cmd(Scripts[i], RemoteScriptFiles[i]));
            List<string> Track = RunCommands(commands);
        }

        public static string Create_TextFile_Cmd(string Text, string RemoteName)
        {
            string tCmd = $"printf \"{Text}\" > {RemoteName}";
            return tCmd;
        }

        public string CreateTextFile(string Text, string RemoteName)
        {
            string tCmd = Create_TextFile_Cmd(Text, RemoteName);
            return RunCommand(tCmd);
        }

        public string RunCommand(string Command, bool ShowError = true)
        {
            return RunCommands(new List<string>(1) { Command }, ShowError)[0];
        }

        public List<string> RunCommands(List<string> Commands, bool ShowError = true)
        {
            List<string> Track = new List<string>();
            string output;
            using (SshClient client = new SshClient(hostname, username, password))
            {
                SshCommand c;
                client.Connect();
                for (int i = 0; i < Commands.Count; i++)
                {
                    c = client.RunCommand(Commands[i]);
                    output = ShowError ? $"{c.Result}|{c.Error}" : c.Result;
                    Track.Add(output);
                    System.Threading.Thread.Sleep(2);
                }
                client.Disconnect();
            }
            return Track;
        }

        #endregion
    }

    // Keep this as a good reference
    public class SSH_Base
    {
        public string hostname = "";
        public string username = "";
        public string password = "";

        public bool Download_Folder(string RemoteFolderPath, string LocalFolderPath)
        {
            if (!RemoteFolderPath.EndsWith("/")) RemoteFolderPath += "/";
            //https://searchcode.com/codesearch/view/10948381/
            try
            {
                using (var scp = new ScpClient(hostname, username, password))
                {
                    scp.Connect();
                    DirectoryInfo DI_Down = new DirectoryInfo(LocalFolderPath);
                    scp.Download(RemoteFolderPath, DI_Down);

                    scp.Disconnect();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Download_File(string RemoteFilePath, string LocalFilePath)
        {
            try
            {
                //https://searchcode.com/codesearch/view/10948381/
                using (var scp = new ScpClient(hostname, username, password))
                {
                    scp.Connect();
                    
                    FileInfo FI_Down = new FileInfo(LocalFilePath);
                    scp.Download(RemoteFilePath, FI_Down);

                    scp.Disconnect();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Upload_File(string RemoteFilePath, string LocalFilePath)
        {
            try
            {
                //https://searchcode.com/codesearch/view/10948381/
                using (var scp = new ScpClient(hostname, username, password))
                {
                    scp.Connect();

                    FileInfo FI_Up = new FileInfo(LocalFilePath);
                    scp.Upload(FI_Up, RemoteFilePath);

                    scp.Disconnect();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Upload_Files(List<string> RemoteFilePaths, List<string> LocalFilePaths)
        {
            try
            {
                //https://searchcode.com/codesearch/view/10948381/
                using (var scp = new ScpClient(hostname, username, password))
                {
                    scp.Connect();
                    FileInfo FI_Up;
                    for (int i = 0; i < RemoteFilePaths.Count; i++)
                    {
                        FI_Up = new FileInfo(LocalFilePaths[i]);
                        if (FI_Up.Exists)
                            scp.Upload(FI_Up, RemoteFilePaths[i]);
                    }
                    scp.Disconnect();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string Create_TextFile_Cmd(string Text, string RemoteName)
        {
            string tCmd = $"printf \"{Text}\" > {RemoteName}";
            return tCmd;
        }

        public string Create_TextFile(string Text, string RemoteName)
        {
            string tCmd = Create_TextFile_Cmd(Text, RemoteName);
            return RunCommand(tCmd);
        }

        public string RunCommand(string Command)
        {
            return RunCommands(new List<string>(1) { Command })[0];
        }

        public List<string> RunCommands(List<string> Commands)
        {
            List<string> Track = new List<string>();
            using (SshClient client = new SshClient(hostname, username, password))
            {
                SshCommand c;
                client.Connect();
                for (int i = 0; i < Commands.Count; i++)
                {
                    c = client.RunCommand(Commands[i]);
                    Track.Add(c.Result + "|" + c.Error);
                    System.Threading.Thread.Sleep(2);
                }
                client.Disconnect();
            }
            return Track;
        }
    }

}

// https://codebeta.com/renci-ssh-for-net-5e07641f5851
// https://github.com/sshnet/SSH.NET/issues/232

